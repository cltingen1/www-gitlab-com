---
layout: job_family_page
title: Engineering Fellow
---

The Engineering Fellow role is the individual contributor equivalent to a VP of Engineering.

## Job Grade
The Engineering Fellow is a [grade 12](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Responsibilities

* Advocate for improvements to product quality, security, and performance that impact all of Engineering at GitLab.
* Solve technical problems of the highest scope and complexity for the entire organization.
* Exert significant influence on the over GitLab's long-term goals and execution.
* Ensure that our standards for style, maintainability, and best practices are suitable for the unique problems of scale and diversity of use represented by the GitLab product. Maintain and advocate for these standards through code review.
* Drive innovation across Engineering with a willingness to experiment and to boldly confront problems of immense complexity and scope.
* Drive significant improvements for GitLab's use, ease of development, and technical efficiency through effective collaboration and prolific technical contributions.
* Represent GitLab and its values in public communication in all aspects of our software development lifecycle and public relations. Interact with customers and other external stakeholders as a consultant and spokesperson for critical projects and aspects of our technical architecture.
* Provide mentorship for Senior and Staff Engineers at the company to help them grow in their technical responsibilities and to share your great expertise across the organization.
* Ship extremely high-impact features and improvements with minimal guidance and support from other members of the organization.
* Help create the sense of psychological safety in the department

## Levels

* Distinguished Engineer
* Engineering Fellow

## Performance Indicators

* TBD

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 60 minute first interview with a VP of Development
* Next, candidates will be invited to schedule a 45 minute second interview with an Engineering Fellow
* Next, candidates will be invited to schedule a 45 minute third interview with another member of the Engineering team
* Next, candidates will be invited to schedule a 45 minute fourth interview with a Director of Engineering
* Next, candidates will be invited to schedule a 45 minute fifth interview with a Chief Technology Officer
* Finally, candidates may be asked to schedule a 50 minute final interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).