---
layout: handbook-page-toc
title: "Risk and Field Security Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc .hidden-md .hidden-lg}
 
## Risk and Field Security Mission
 
The Risk and Field Security team serves as the public representation of GitLab's internal Security function. The team is tasked with providing high levels of security assurance to internal and external customers. We work with all GitLab departments to document requests, analyze the risks associated with those requests, and provide value-added remediation recommendations.

Hear more about our team and how we support GitLab in this [interview with the Manager of Risk and Field Security.](https://www.youtube.com/watch?v=h95ddzEsTog). 
 
## Risk and Field Security Primary Functions

### Field Security
In support of GitLab’s Sales and Customer Success Teams, the Risk and Field Security team maintains the [Customer Assurance Activities Procedure](/handbook/engineering/security/security-assurance/risk-field-security/customer-security-assessment-process.html) for the intake, tracking, and responding to GitLab Customer and Prospect Security Assurance Activity request. This includes, but is not limited to: 
 
- Proactively maintaining self-service security and privacy resrouces including the [Customer Assurance Package](/handbook/engineering/security/security-assurance/risk-field-security/customer-assurance-package.html), the [Trust Center](https://about.gitlab.com/security/) and the **internal only** [Answer Base](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/common-security-questions.html).
- Completing Security, Privacy, and Risk Management Questionnaires
- Assisting in Contract Reviews
- Participating in Customer Meetings
- Providing External Evidence (such as GitLab’s SOC2 Penetration Test reports)
- Conducting Annual [Field Security Study](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/procedures/field-security-study.html)
 
Requests for Customer Assurance Activities should be submitted using the **Customer Assurance** workflow in the `#sec-fieldsecurity` Slack Channel. Detailed work instructions are located in the [Field Security Project](https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/risk-field-security#customer-assurance-activities). 
 
<!-- blank line -->
----
<!-- blank line -->

### Third Party Risk Management
Whenever a Third Party is introduced into the GitLab environment, there is a risk that their security posture can negatively impact GitLab. In order to reduce this risk, the Risk and Field Security team maintains the [Third Party Risk Management Procedure](/handbook/engineering/security/security-assurance/risk-field-security/third-party-risk-management.html) for the intake, assessment, tracking and responding to GitLab Third Party Risk Management requests. 

<!-- blank line -->
----
<!-- blank line -->
### Security Operational Risk Management
Focused on Tier 2/Operational Risks, the Risk and Field Security team maintains the [Security Operational Risk Management Procedure- StORM](/handbook/engineering/security/security-assurance/risk-field-security/risk-management.html) for the intake, assessment, tracking and responding to GitLab StORM requests. 

**Need to communicate a potential risk to the team?** Please refer to the various communication methods documented on the [StORM Program](/handbook/engineering/security/security-assurance/risk-field-security/risk-management.html#communication-of-risks-to-the-risk--field-security-team) handbook page.

<!-- blank line -->
----
<!-- blank line --> 
## Metrics and Measures of Success

`In Progress`
 
## Makeup of the team
- [Devin Harris](/company/team/#dsharris) @dsharris
- [Marie-Claire Cerny](https://about.gitlab.com/company/team/#marieclairecerny) @marieclairecerny 
- [Steve Truong](https://about.gitlab.com/company/team/#sttruong) @sttruong
- [Darren Laminson White](https://about.gitlab.com/company/team/?department=security#dlwhite0322) @dlwhite0322
- [Meghan Maneval](https://about.gitlab.com/company/team/?department=security-department#mmaneval20) @mmaneval20
- [Julia Lake](https://about.gitlab.com/company/team/#julia.lake) @julia.lake
 
## References
* [Customer Assurance Activities Procedure](/handbook/engineering/security/security-assurance/risk-field-security/customer-security-assessment-process.html)
* [Third Party Risk Management Procedure](/handbook/engineering/security/security-assurance/risk-field-security/third-party-risk-management.html)
* [Security Operational Risk Management Procedure](/handbook/engineering/security/security-assurance/risk-field-security/risk-management.html)
 
## Contact the Field Security Team
* GitLab Issues
  * `@gitlab-com/gl-security/security-assurance/risk-field-security-team`
* Email
  * `fieldsecurity@gitlab.com`
* Slack
  * Group Handle
    * `@field-security`
  * Primary Slack Channels
    * `#sec-fieldsecurity`
    * `#sec-risk-mgmt`
