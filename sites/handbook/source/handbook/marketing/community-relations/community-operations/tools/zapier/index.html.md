---
layout: handbook-page-toc
title: "Community advocacy tools: Zapier"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Zapier subscription

We use Zapier to automate the task of identifying mentions of GitLab across our [Community Response channels](/handbook/marketing/community-relations/community-advocacy/#community-response-channels) and generally convert them to actionable Zendesk tickets

The Zapier subscription runs on the [Team](https://zapier.com/app/billing/plans) plan, and is shared with the rest of the GitLab team

### Zapier access

- URL: [https://zapier.com](https://zapier.com)
- Account: search for the shared Zapier account in 1Password's `Team` vault 

Once logged in, you can access, edit and create Zaps in the [Community Relations](https://zapier.com/app/zaps/folder/275996) folder

<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59)
;"></i> After editing or creating a new Zap, remember to turn it on with the toggle switch next to the task's name on the Zap's list.
{: .alert .alert-warning}

### Current Zaps

List of Zaps that are enabled on the [Community Relations folder](https://zapier.com/app/zaps/folder/275996).

| Zap | Description |
| --- | --- |
| [about.gitlab.com + devops-tools comments](https://zapier.com/app/editor/47854205) | GitLab blog comments to #mentions-of-gitlab Slack channel, and DevOps Tools pages comments to Zendesk and #devoptools-comments Slack channel |
| [Community Relations MR Updates](https://zapier.com/app/editor/92097020) | All merged MRs with `Community Relations` label posted to #community-relations Slack channel |
| [Hackernews: Slack notifications for front page mentions](https://zapier.com/app/zaps/folder/275996) | Hackernews: front page stories to #developer-evangelism Slack channel |
| [Hackernews: Slack Notifications for mentions](https://zapier.com/app/editor/52810208) | Hackernews: mentions to #hn-mentions Slack channel |
| [GitLab Swag Store](https://zapier.com/app/editor/18836033) | GitLab store order to Printfection order |
| [Student Spotlights Application Alerts](https://zapier.com/app/editor/90643179) | Student Spotlights form submission notifications to #student-spotlights-applications |


