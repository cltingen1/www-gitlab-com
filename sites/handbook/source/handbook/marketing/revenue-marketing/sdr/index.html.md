---
layout: handbook-page-toc
title: "Sales Development"
description: "As a Sales Development Representative (SDR), you focus on outreach, prospecting, and lead qualification."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

As a Sales Development Representative (SDR), you focus on outreach, prospecting, and lead qualification. To do so, you need to have an understanding of not only product and industry knowledge, but also sales soft skills, and internal tools and processes. This handbook page will act as a guide to those topics as well as general information about the SDR team.

## Reaching the Sales Development Team (internally)

#### Slack Channels (by region)

* **Main Channel- (Global Manager - Hannah Woodward)** = [`#sdr_global`](https://gitlab.slack.com/messages/C2V1KLY0Z)      
* **Announcements - (SDR Enablement - Chris Wang)** = [`#sdr-fyi`](https://app.slack.com/client/T02592416/C011P828JRL)       
* **Conversations** = [`#sdr-conversations`](https://gitlab.slack.com/messages/CD6NDT44F)       

**AMER**
* **All** = [`#sdr_amer`](https://gitlab.slack.com/messages/CM2GAVC78)      
* **AMER SMB + LATAM + Public Sector (Manager- Allison Graban)** = [`#sdr_amer_small_latam_pub`](https://app.slack.com/client/T02592416/C013W1DH98Q)
* **AMER Mid-Market (Manager - Sunanda Nair)** = [`#sdr_amer_mm`](https://app.slack.com/client/T02592416/C014PHFNE2U)
* **Enterprise Expand (Manager - Interim and Lead Expand Manager - Elsje Smart)** = [`#sdr_amer_ent_expand`](https://gitlab.slack.com/archives/CUFRP6U6Q)
* **Enterprise Land East (Manager - Maggie Barnes)** = [`#sdr_amer_ent_land_east`](https://gitlab.slack.com/archives/C01LK4EFDNW)
* **Enterprise Land West (Interim and Lead Land Manager - Meaghan Thatcher)** = [`#sdr_amer_ent_land_west`](https://gitlab.slack.com/archives/C01LFGBSVV3)

**EMEA**
* **All** = [`#sdr_emea`](https://gitlab.slack.com/messages/CCULKLB71)      
* **Commercial - (Manager - Panos Rodopoulos)** = [`#sdr_emea_commercial`](https://gitlab.slack.com/messages/CM0BYV7CM)
* **Enterprise Expand - (Interim and Lead Expand Manager - Elsje Smart)** = [`#sdr_emea_expand`](https://gitlab.slack.com/archives/C01LHENNLKD)
* **Enterprise Land - (Manager - Miguel Nunes)** = [`#sdr_emea_land`](https://gitlab.slack.com/archives/C01M76N9NTA)

**APAC**
* **All - (Manager - Glenn Perez)** = [`#sdr_apac`](https://gitlab.slack.com/messages/CM0BPBEQM)

**Please acknowledge any Slack messages from your managers in your Slack channels, if you have nothing to say at least be creative and funny with emojis so that they know that you are seeing their requests and updates and are talking to an empty room!** 

#### Issue Boards & Team Labels 

[SDR Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/707128): used to track relevant GitLab issues involving the SDR team. This is a global issue board and will capture all issues in any group/sub-group in the GitLab.com repo when any of the following *scoped* labels are used. 

[SDR Event Tracker](https://gitlab.com/groups/gitlab-com/-/boards/1718115): used to track upcoming events globally.

- `SDR` - issues concerning the SDR team. This label will typically be removed and changed to one of the below labels once accepted by our team.
- `SDR::Priority` - projects that we would like brought into RevOps meeting for feedback/next steps from other teams
- `SDR::Planning` - Discussion about next steps is in progress for issues concerning the SDR team
- `SDR::In Progress` - SDR action item is presently being worked on
- `SDR::On Hold` - SDR project is put on hold after agreement from SDR leadership team 
- `SDR::Watching` - No direct SDR action item at this time, but SDR awareness is needed for potential support/questions
- `SDR::Enablement Series` - Label to track and monitor upcoming topics for the SDR enablement series. All of these issue roll up to this epic.
- `SDR::AMER Event Awareness` - Americas SDR awareness is needed for potential support/questions in regard to events
- `SDR::APAC Event Awareness` - APAC SDR awareness is needed for potential support/questions in regard to events
- `SDR::EMEA Event Awareness` - EMEA SDR awareness is needed for potential support/questions in regard to events
- `SDR Pub Sec` - PubSec SDR awareness is needed for potential support/questions in regard to events
- `SDR West Staff Request` - Utilized when a West SDR needs to be assigned to an issue

## Quick Reference Guide

| Resource |
| :----: | 
|  [SDR onboarding page](/handbook/marketing/revenue-marketing/sdr/sdr-playbook-onboarding/) | 
|  [Tanuki Tech](/handbook/marketing/revenue-marketing/sdr/tanuki-tech/) |
|  [Sales handbook page](/handbook/sales/) | 
|  [Sales resources page](/handbook/sales/#quick-reference-guide) | 
|  [Go to Market page](/handbook/sales/field-operations/gtm-resources/) | 
|  [SDR Manager resources page](/handbook/marketing/revenue-marketing/sdr/sdr-manager-resources) | 
|  [SDR job family/levels](https://about.gitlab.com/job-families/marketing/sales-development-representative/) | 

## Onboarding
In your first month at GitLab we want to help ensure you have everything you need to be successful in your job. You will go through enablement videos, live sessions and activities covering a wide range of getting started topics. 
- [SDR onboarding goals and process](/handbook/marketing/revenue-marketing/sdr/sdr-playbook-onboarding/)

## SDR Training & Development

#### SDR Enablement

The SDR team will conduct enablement and training on a wide range of topics such as workflow/processes, campaigns, alignment with other teams, tool training, product training, etc. SDR enablement sessions are scheduled on an as needed basis and will be made available for SDR teams to consume asynchronously.

To view previously recorded SDR Enablement content, you can view the [SDR Enablement Video Library here](https://www.youtube.com/playlist?list=PL05JrBw4t0KrjbznnEEiCtxUfT8-OV6X8) 

- If you would like to request or run an enablement session on a specific topic, please fill out [this issue](https://gitlab.com/gitlab-com/marketing/sdr/-/issues/new?issuable_template=sdr_enablement_series_request).

#### SDR Technical Development
As part of your [onboarding](/handbook/marketing/revenue-marketing/sdr/sdr-playbook-onboarding/), you will begin an SDR Technical Development course with our Senior Sales Development Solutions Architect. The goal of this course is to enable you to be more comfortable having technical discussions - specifically when it comes to GitLab’s use cases. Each level of the course is tied to our [levels](https://about.gitlab.com/job-families/marketing/sales-development-representative/#levels) in the SDR role.

## Making Changes to the GitLab Handbook (for SDRs)

[Video Walkthrough of how to make changes to the GitLab Handbook for the SDR org](https://drive.google.com/file/d/1BjvMNgQgimYZOnaX0NpgaFvbYl47Jcwa/view?usp=sharing)

One of our Values is being [handbook first](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/). In order to align the SDR organization more closely to this ideal, below are suggested steps. Please remember that the Handbook is a living document, and you are strongly encouraged to make improvements and add changes. This is ESPECIALLY true when it comes to net new solutions that should be shared so that the whole organization has access to that process. (aka The DevOps ideal of turning "Localized Discoveries" into "Global Knowledge".)

Steps:

1. Have a change you want to make to the handbook? Great!
2. Navigate to the source code of the page in the handbook (e.g. [Link to edit the SDR page in the Handbook](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/handbook/marketing/revenue-marketing/sdr/index.html.md) )
3. Click either "Edit" or "Web IDE" to make your changes.
4. Have a brief but descriptive "Commit message" (e.g. "Add new section on 'How to Make Changes to the Handbook'") and commit your changes
5. Fill out the Merge Request details

## SDR Resources

| SDR Resources    |  Description       | 
| :---- | :---- |  
|  [SDR job family](https://about.gitlab.com/job-families/marketing/sales-development-representative/) | *Information on the SDR role and requirements for level progression* |
|  [SDR onboarding page](/handbook/marketing/revenue-marketing/sdr/sdr-playbook-onboarding/) | *Overview of SDR onboarding that includes an overview, process, expectations, and manager responsibilities* |
|  [SDR Manager resources page](/handbook/marketing/revenue-marketing/sdr/sdr-manager-resources) | *SDR Manager resources & links* |
|  [SDR Territory Alignment](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#sdr-team-overview)| *Currently the single source of truth for SDR/Territory Alignment, Public Sector SDR territory [**here**](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#pubsec-amer-sdrs)* |
|  [Enterprise SDR Outbound Process Framework](https://drive.google.com/drive/search?q=%20Outbound%20Process%20Framework%22%40gitlab.com) | *Outbound process framework for the Enterprise SDR team. Note: These vary by team and geo*|
| [SDR Outreach Sequence Library](https://drive.google.com/drive/u/0/search?q=%22sdr%20outreach%20sequence%20library%22) | *Approved Outreach sequences for GitLab value drivers, use cases, personas, and campaigns* | 
|  [SDR Enablement Videos](https://www.youtube.com/playlist?list=PL05JrBw4t0KrjbznnEEiCtxUfT8-OV6X8)| *Enablement videos and how-tos for SDRs* |

| Marketing Resources    |  Description       | 
| :---- | :---- |  
|  [Marketing Resource Links](https://docs.google.com/spreadsheets/d/1NK_0Lr0gA0kstkzHwtWx8m4n-UwOWWpK3Dbn4SjLu8I/edit?usp=sharing)| *GitLab whitepapers, ebooks, webcasts, analyst reports and more for Sales & SDR education*|
|  [Marketing Events + Gated Content Assets + Webcasts](https://drive.google.com/drive/search?q=%22Events%20Gated%20Content%20Assets%22%20owner:jgragnola%40gitlab.com) | *SDRs can use this sheet to refer better understand the marketing assets that are being consumed by prospects. To view the ungated content, click on the link in the Pathfactory or PDF/YouTube columns. Note: Sharing non-gated assets requires SDR manager approval* |
|  [GitLab Buyer Personas](/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/)| *Resource to help GitLab sellers better understand our buyers and audiences*

| Sales Resources    |  Description       | 
| :---- | :---- |  
|  [Sales handbook page](/handbook/sales/) | *GitLab Sales team handbook* |
|  [Sales resources page](/handbook/sales/#quick-reference-guide) | *Link to the Sales quick reference guide with links to learn more about the various sales teams & initiatives* |
|  [Weekly sales enablement](/handbook/sales/training/sales-enablement-sessions/) | *These sessions take place every Thursday and SDRs have an open invitation to participate* |
|  [Go to Market page](/handbook/sales/field-operations/gtm-resources/) | *Link to GitLab's go-to-market strategy including: definitions, processes, and record management* |
|  [Sales Training handbook page](/handbook/sales/training/) | *Link to GitLab sales training* | 
|  [Command of the Message](/handbook/sales/command-of-the-message/) | *"Command of the Message" training and the GitLab value framework* |
|  [Most commonly used sales resources](/handbook/marketing/strategic-marketing/sales-resources/)| *Sales resources page*
|  [Flash Field newsletter](/handbook/sales/field-communications/field-flash-newsletter/)| *Learn more about sales' weekly newsletter*

## Segmentation
The SDR team aligns to the [Commercial](/handbook/sales/commercial/) and Large sales teams. These teams are broken down into three segments: [Large, Mid-Market and SMB](/handbook/sales/field-operations/gtm-resources/#segmentation) which are based on the total employee count of the Global account. *Note: The commercial sales team includes both Mid-Market and SMB. This segmentation allows SDRs and Sales to be focused in their approach and messaging. The segments are aligned to a region/vertical and then divided one step further via territories in the regions. Our single source of truth for determining number of employees is DataFox followed by Zoominfo. 
* [Sales segmentation](/handbook/sales/field-operations/gtm-resources/#segmentation)
* [Sales territories](/handbook/sales/territories/)
* [Determining if a lead is in your territory](handbook/sales/field-operations/gtm-resources/rules-of-engagement/)
* [SDR and Sales alignment](/handbook/marketing/revenue-marketing/sdr/sales-sdr-alignment/) - working with your SAL or AE!

**SDR Team Breakdown**
     *  AMER SMB
     *  AMER Mid-Market
     *  AMER Enterprise Expand
     *  AMER Enterprise Land East
     *  AMER Enterprise Land West
     *  EMEA Enterprise Expand
     *  EMEA Enterprise Land
     *  EMEA Commercial (MM & SMB)
     *  APAC     

## SDR Compensation and Quota

SDR quota is made up of the following depending on [sales segment](/handbook/marketing/revenue-marketing/sdr/#segmentation):
- [Sales Accepted Opportunities (SAOs)](/handbook/sales/field-operations/gtm-resources/#criteria-for-sales-accepted-opportunity-sao)
- [Initial Qualified Meetings (IQM)](/handbook/marketing/revenue-marketing/sdr/#qualified-meeting)
- [ARR](/handbook/sales/sales-term-glossary/arr-in-practice) pipeline component  
- 2-way communication must be documented on the Contact in the Opportunity to receive credit. Opportunities missing this documentation will not be considered for compensation and will not retire quota. 
- For SDRs who have SAO/ARR goals, compensation is based on the SAO attainment. ARR attainment is a qualifier for accelerator payments. Our aim is to land net new logos and expanding to new customer business units, SAOs being our quantity metric and ARR our quality metric.  Our mission is to create good qualified pipeline for the company.
- SDRs are not responsible for uptiering customer accounts from a paid lower tier to a higher tier, ie. from Premium to Ultimate, this is the responsibility of sales. 

## Activity & Results Metrics

While the below measurements do not impact your quota attainment, they are monitored by SDR leadership. 

**Results**
  * ARR won from opportunities SDR sources

**Activity**
  * Number of opportunities created
  * Number of calls made
  * Number of personalized emails sent
  * LinkedIn InMails
  * Drift Chats
  * Number of leads accepted and worked
  

  **Daily outbound metrics** 
    * We aim for 45 touchpoints per day using the various meathods above.  This is a broad expectation and may vary per team given the segment, functionality and strategy of the team manager.
    

## SDR Standards
* Meet monthly quota of Sales Accepted Opportunities (SAOs) and Initial Qualifying Meetings (IQMs) and ARR pipeline targets you have been set.
* Be able to identify where a prospective customer is in their buying cycle and take appropriate action to help them along their journey towards becoming a customer.
* Have a sense of urgency - faster response time directly influences conversion rates.
* Work lead records within Salesforce by leveraging sequences in Outreach.
* Maintain a sense of ownership of data integrity in Salesforce and Outreach: cleaning up and assuring accuracy and consistency of data.
* Adding any information gathered about a LEAD, CONTACT, or ACCOUNT from the data sources Datafox & Zoominfo where you can to SFDC.
* Attend each initial qualifying meeting (IQM) with your AE/SAL. Documenting notes in SFDC, including adding the Contacts you have been in communication with and attaching all corresponding connects and attempts to the Contact record, lastly communicating with your AE/SAL before and after the meeting.
* While we have a "no ask, must tell" PTO policy, we ask that in accordance with the [Time Off Policy](https://about.gitlab.com/handbook/paid-time-off/#a-gitlab-team-members-guide-to-time-off) in the handbook that you give your manager a heads up early of your intended time off so that they can help arrange cover for your tasks, if necessary. If you are taking off more than one day, please give your manager at least a week's notice.

## SDR Tools

| Tool | Intro Overview  | Advanced Training  | Best Practices Resources  |
|---|---|---|---|
| [SalesForce](/handbook/business-technology/tech-stack/#salesforce) | coming soon  | coming soon  | coming soon  |
| [Outreach.io](/handbook/business-technology/tech-stack/#outreachio) | [Slide deck](https://docs.google.com/presentation/d/1IpTVE4-Nkblfuiu6f1OUnFbi8IpjRPK7UDSx4eSfQCg/edit#slide=id.g5a343b482a_2_10) and [Training video](https://outreach.zoom.us/rec/share/yFNlVvUoyHeBB4-39mgai2FsMy3v6BR2mhBgrljFosG6EE8zExR3xVHc-T7XFZPc.JJRvDk-7pArZa3Sr) *pswd in onboarding issue or ask manager  | coming soon | [Our setup and process](/handbook/marketing/marketing-operations/outreach/)  |
| [Datafox](/handbook/business-technology/tech-stack/#datafox) | coming soon  | coming soon | coming soon  |
| [ZoomInfo](/handbook/business-technology/tech-stack/) | [40 min ZI University Video](https://university.zoominfo.com/learn/course/101-introduction-to-zoominfo-on-demand/training-session/101-introduction-to-zoominfo) | coming soon | [Quick saved searches](https://www.youtube.com/watch?v=OpTgvoOQ1jM) - [Leveraging List Match](https://www.youtube.com/watch?v=vl1kpsNa874) - [Tagging Prospects in LinkedIn](https://www.youtube.com/watch?v=GQWTZWbMgg8) |
| [LinkedIn Sales Navigator](/handbook/business-technology/tech-stack/#linkedin-sales-navigator) | [70 min Navigator Tutorial](https://www.linkedin.com/learning/learn-linkedin-sales-navigator-3/welcome-to-sales-navigator?u=2255073) | coming soon | [Peer Tips](https://drive.google.com/file/d/1xzz8cEiSFqZ7bOw-dpqoNlHjSDwrIFE4/view) - [LI Personalizing](https://www.youtube.com/watch?v=7vEVB-WgDPA) - [Loading Accounts from SFDC into LI](https://www.youtube.com/watch?v=_D8walmmOAU) |
| [Chorus](/handbook/business-technology/tech-stack/#chorus) | [Tips for getting started](https://docs.chorus.ai/hc/en-us/articles/115009183547-Tips-on-Getting-Started-with-Chorus) | coming soon | coming soon  |
| [Drift](/handbook/business-technology/tech-stack/#drift) | [50 min Training](https://drive.google.com/drive/u/0/folders/1aGbQopASH0y540by0BS4UQQkjAL9bvyD) | coming soon  | [SDR Flow/ Example Conversations](https://docs.google.com/document/d/1EQYVu2cO-Zn1T2EuTQrIG4z-pRZucs7FG0rhQQo4wzs/edit) |
| [LeanData](/handbook/business-technology/tech-stack/#lean-data) | NA | [Routing workflows](/handbook/marketing/marketing-operations/leandata/) | NA |
| [Demandbase Sales Conversion](/handbook/marketing/revenue-marketing/account-based-strategy/#demandbase) | [50 min SDR Training Video](https://www.youtube.com/watch?v=R3uwFAMhsHM) and [Slide Deck](https://docs.google.com/presentation/d/1qJNxl503zO0x5UuUz-TOe5Lt_jWQa_Hgz-UrNCQqjOk/edit#slide=id.g29a70c6c35_0_68) | HB process page  | [4 min Creating an Account List video](/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/#creating-a-list-video) |

## SDR Workflow & Process
As an SDR, you will be focused on leads - both inbound and outbound. At the highest level, a lead is a person who shows interest in GitLab through inbound lead generation tactics or through outbound prospecting.
- [Updating](/handbook/marketing/revenue-marketing/sdr/sdr-manager-resources/#lead-routing--alignment-resources) territory alignment, `SDR Assigned` field, lead routing, Drift alignment/team 

### Qualified Meeting

**What is considered a Qualified Meeting?**

A Qualified Meeting is a meeting that occurs with a prospect who is directly involved in a project or team related to the potential purchase of GitLab within a buying group, either as an evaluator, decision maker, technical buyer, or *influencer* within an SDR Focus Account. 
To be considered qualified, the meeting must occur (prospect attends) and new information must be uncovered that better positions the SAL and Regional SDR to create an SAO with that particular account based on the [SAO Criteria](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#criteria-for-sales-accepted-opportunity-sao) as stated in the handbook. The Required IQM Fields must also be populated in order to receive IQM meeting credit.

**Qualified Meeting Process**

A “New Event” IQM gives an SDR the ability to track every meeting that is scheduled and held in Salesforce without having to prematurely create a net new opportunity. SDRs can tie any event (IQM) to an existing account, opportunity, etc. The purpose of creating an IQM in salesforce is to provide the ability to track how many IQMs are being created per SDR and what the results are of these IQMs. 

The purpose of creating an Initial Qualifying Meeting (IQM) in Salesforce is to track top of funnel pipeline meetings that are booked, occur, and convert to Sales Accepted Opportunities (SAOs). SDRs can track every meeting that is scheduled and held in Salesforce without having to prematurely create a net new opportunity prematurely. SDRs can tie any IQM to an existing account or opportunity from a Lead or Contact record. 

**How to create an IQM in SFDC**

Within Salesforce there is a button titled “New Event” which you can access in the global actions bar on your Salesforce “Home Screen”, and in the “Open Activity” section of any Lead, Contact, Account, and Opportunity record. [This image](https://docs.google.com/document/d/1NUP7Ze_e-1sKGAELim-_yaLDnN9rf0wVKWfug0lhtyI/edit?usp=sharing) will show you where to click!

If creating an Event: IQM under a Lead or Contact record, click the event button in the global actions bar and complete all relevant fields which should include your pre-IQM notes in the description field. 

Once a meeting with a prospect is confirmed, the SDR will send either a Google calendar invite to the prospect (include their SAL) and/or send a meeting in Outreach.io with a proposed agenda. SDR should share Event IQM SFDC link with SAL prior to IQM occurring - scheduling a pre/post IQM sync is best practice. 

Once the IQM occurs, then the SDR can add their Post-IQM notes to the Description Field and update the Event Disposition accordingly. If the IQM meets criteria and has next steps, then it's an SAO and the SDR should convert Lead to Contact record and create new opportunity with same process as outlined in Handbook. If the IQM occurred and the SDR/SAL/AE uncovered helpful information but ultimately, there are no next steps, the the SDR should disposition Event IQM as Qualified Meeting (do not create a new opportunity). If Event IQM was a no-show and/or needs to be rescheduled, then SDR owns reaching out to prospect to reschedule and dispositions Event IQM accordingly. 

Depending on Sales Segment the SDR covers they will have IQM quota tied to their compensation.  In order to track those results and report on attainment vs. the goal the SDR must have their name in the Assigned To field on the IQM in Salesforce.

**How to Create an IQM in Outreach**

In Outreach, you have the ability to book a meeting directly from a prospect overview. Even better, this can be done while you’re on a call with a prospect or immediately after without having to leave Outreach.

Select “book meeting” from the prospect actions menu. For a visual/video walkthrough of this process take a look [here](https://docs.google.com/document/d/1oB3RsjqCIUoXvX3F4DTrYOLh4X0K-7tzUPs3jYAQrwg/edit?usp=sharinghttps://docs.google.com/document/d/1oB3RsjqCIUoXvX3F4DTrYOLh4X0K-7tzUPs3jYAQrwg/edit?usp=sharing).

Note: Booking a meeting from Outreach will automatically pull the following fields directly into an SFDC event object:
* Subject (IQM - Lead/Contact Name - Value Driver / Use Case)
* Location (Zoom Link)
* Start & End times
* Assigned To (SDR name) - Required for Quota Retirement
* Event Disposition (Required)
* Activity Source (Required)
* Booked By
* Attributed Sequence Name
* Description (add Pre/Post IQM Notes)
* Related To (You can select a parent opportunity if applicable or relate to the opportunity you create from Qualified Meetings)

#### IQM Requirements (Enterprise & Mid-Market)
The following fields are required and should be updated in real-time as IQMs are scheduled, occur, and convert to SAOs.
* **Subject:** _IQM - Lead/Contact Name - Value Driver / Use Case_
* **Activity Source:** _Drop down, selection required_
* **Call Recording:** _Attach if call was recorded_
* **Assigned To:** _SDR name_ - Required for Quota Retirement
* **Type:** _= IQM_
* **Booked By:** _SDR Name_
* **Attributed Sequence Name:** _This required field is used to determine whether the IQM was sourced via inbound lead or outbound prospecting as well as the sequence used to hook the prospect to take a meeting. Please use the following naming conventions when filling out this field (Note: IB = Inbound / OB = Outbound):_
   - "IB: Sequence Name as it appears in Outreach"
   - "IB: No Sequence"
   - "OB: Sequence Name as it appears in Outreach" 
   - "OB: No Sequence"
* **Event Disposition:** _This field should be kept up-to-date at all times. If the meeting is scheduled (on the calendar) but has not occurred, please use disposition "None." As soon as a meeting occurs, no shows, or reschedules the SDR should update the disposition to accurately reflect the current IQM stage_
* **Description:** _Use the following format/sections to outline notes on the opportunity. The first four sections (Authority, Initiative, Fit, and Timing) are required for SAO credit, but do not need to be completed to receive IQM credit._
   - Authority:
   - Timing:
   - Fit:
   - Initiative: 
   - Next Steps: 
   - Additional Notes (strongly encouraged, but not required)
      - Prospect's title
      - Prospect's LinkedIn
      - Challenges / Pain points
      - Value Driver / Use Case of interest
      - Team details / potential # of users / team size
      - Urgency: Why now?
      - Why GitLab?
      - Competitors 
      - Questions asked
      - Questions I want to ask
* **Persona Functions:** _Drop down, selection required_
* **Persona Levels:** _Drop down, selection required_
* **Name:** _Direct link to the lead/contact associated with the IQM_
* **Related To:** _Link to Opportunity upon conversion to SAO. You can select a parent opportunity if applicable or relate to the opportunity you create from Qualified Meetings_

### Working Inbound Leads

#### What is an MQL?
The GitLab marketing team uses digital channels - social media, email, mobile/web apps, search engines, websites, etc - as well as in-person marketing activities to meet potential buyers where they are. When people interact with GitLab, we use lead scoring to assign a numerical value, or points, to each of these leads based on their actions and the information we have about them. Once a lead reaches 100 points, they are considered a [Marketing Qualified Lead](/handbook/sales/field-operations/gtm-resources/#mql-definition-and-scoring) or MQL.

#### Inbound Process
SDRs are responsible for following up with MQLs by reviewing their information, reaching out, and working with them to understand their goals, needs, and problems. Once you have that information, you can use our [qualification criteria](/handbook/sales/field-operations/gtm-resources/#criteria-for-sales-accepted-opportunity-sao) to determine if this is someone who has strong potential to purchase our product and therefore should be connected with sales for next steps. As you are the connection between Marketing and Sales you want to make sure every lead you pass to the Sales team is as qualified as possible.

These MQLs will show up in your lead views in Salesforce. The views, listed below, allow you to see your leads in a categorized way to simplify your workflow. Leads are routed to you and flow into your views via the tool [LeanData](/handbook/marketing/marketing-operations/leandata/) which takes each lead through a series of conditional questions to ensure it goes to the right person. Even though all SDRs leverage the same views, they will only show you leads that have been specifically routed to you. You will be responsible for following up with all of the leads in your MQL views by sequencing them using [Outreach.io](/handbook/marketing/marketing-operations/outreach/). Once sequenced, their lead status will change and they will move from your MQL views allowing you to have an empty view. Managers monitor all views to ensure they are cleared out. If you choose not to work a lead please unqualify it and state your reasons in the lead record.

#### Lead Views
* My MQLs - all leads that have accumulated [90 points based on demographic/firmographic and/or behavioral information](/handbook/marketing/marketing-operations/#lead-scoring-lead-lifecycle-and-mql-criteria). These leads should always be your first priority. 
     * Important to note: If one of your leads submits a Contact Us form on the website, you will receive an email alert notifying you to take action quickly in addition to the lead showing up in your My MQL view.
     * Contact Requests are to be actioned on the same day whenever possible 
     * Other MQLs are to be actioned within 2 business days for Enterprise, and up to 5 business days for Commercial
* My Inquiries - leads that haven't quite reached 90 points yet. These leads are to be looked at once your MQLs are worked and can be sequenced as well. This view is split into three distinct options:
    * `My Inquiries (ALL)`: All Inquiry leads assigned to the SDR will appear in this view, based on the SDR's SFDC login
    * `My Inquries (Job Title)`: This view filters Inquiry leads based on key job titles. It also filters by the lead's activity in the `last 60 days`
    * `My Inquiries (Person Score)`: This view filters by lead activity in the `last 60 days`, by if a lead has a `Behavior Score greater than or equal to 30 pts`, by if a lead has a `Demographic Score greater than or equal to 15 pts` and lastly if a lead is in `Inquiry` status. Please note this view will become more useful over time and after score degradation has been active for longer (current as of Dec 2020)
    * Final note: or Field Events, relevant personas in your inquiries are to be actioned within 5 business days 
* My Qualifying - leads that you are actively qualifying in a back and forth conversation either on email or through phone calls
* My Leads GL4300 & MM4000 - any unconverted leads with a `Matched Account GTM Strategy` of: `Account Centric`or `Account Based - Net New`
* My Leads w/new LIM - Stale MQL, Nurture, Raw w/new LIM - leads that at one point were an MQL or had a status of nurture or raw but have recently taken an action and have a new Last Interesting Moment Description (LIM). 
* My Leads w/ Phone Number - leads that do not have blank `Phone` and `Mobile` fields, have activity within the `last 60 days` and have not given an adequate Nurture reason 

#### Contact Views
* My MQLs - If someone is already a contact in our system and has taken a lead qualifying action or requested contact with a Contact Us form on the website, they will appear in this view. Determine if this contact is closely engaged with Sales and reach out accordingly
* My Qualifying - contacts you are actively qualifying in a back and forth conversation either on email or through phone calls
* My Contacts w/new LIM - Stale MQL, Nurture, Raw w/new LIM - contacts that at one point were an MQL or had a status of nurture or raw but have recently taken an action and have a new Last Interesting Moment Description (LIM). 

#### Lead and Contact Status / Outreach Automation

Lead/contact statuses allow anyone in Salesforce to understand where a lead is at in their journey. The automation mentioned below is all done through an Outreach.io trigger. 

* Once a lead has been placed in an Outreach sequence, the lead status will automatically change from MQL, Inquiry or Raw to Accepted marking that you are actively working this lead. 
* When a lead responds to you via email, their status will again automatically change. This time it will change from Accepted to Qualifying. At this stage you should add the date and notes to the `Qualifying Notes' field with your intended next steps. *If you are not working on qualifying this lead further, you will need to manually change the status to Nurture so that this lead is back in Marketing nurture and isn’t stuck in your My Qualifying view. If you have spoken to the lead by phone and are qualifying them, you need to manually change the status to from Accepted to Qualifying
* If a lead finishes an Outreach sequence without responding, the lead status will automatically change to unresponsive or nurture in three days if there is still no response.
* If a lead responds, SDR is to schedule a call/meeting using [Outreach’s meetings feature.](https://about.gitlab.com/handbook/marketing/marketing-operations/outreach/#Outreach-Meetings-for-SDR-Team)
* The only time you will need to manually change lead status outside of what is mentioned in the previous bullets is if you for some reason don't use an Outreach sequence to reach out to someone or if you need to unqualify a lead for bad data etc.
* If you check the `Inactive lead` or `Inactive contact` checkbox, signifying that this person no longer works at the company, any running sequence will automatically be marked as finished. 

#### Inbound Lead Workflow Checklist

The purpose of the checklist below is to provide a clear workflow of steps to be followed for each MQL or Inquiry lead received. This checklist should help standardize your actions and help ensure that leads in your pipeline match the [Sales organisation's RoE](https://gitlab.com/gitlab-com/marketing/sdr/-/issues/657). You will be using the 'Find Duplicates' and 'LinkedIn' button on the SFDC lead record, along with our SSOT platform, for the workflow below.

** If lead reassignments internally need to happen at any time while following these steps, leverage the instructions in our [SDR Team Overview grid.](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#sdr-team-overview) **

**Step 1**: Verify The Sales Organisation RoE

On SFDC lead record, press `Find Duplicates` and `LinkedIn` and follow the workflow below.

1. **For the Commercial Team**, if existing SFDC account type equals "Customer", press 'Convert to Account and [convert lead to a contact]((https://drive.google.com/file/d/1I4MsYvVdAcxA__jHcKQddlFIpzWnzhA3/view?usp=sharing) for that account. In case of a Contact Request lead source, please also chatter the Account Owner and inform them on the context of the message for them to action.
    i. For the **Large Expand Team**, If an existing SFDC `Customer` account already exists, verify that the SFDC lead/prospect is not part of the existing GitLab subscription before working the lead by sending a short snippet like, "Hi XYZ, Thanks for reaching out. ABC company current has a GitLab subscription, are you currently using a GitLab license and if so, what version?". If they are, please follow the "Convert to Account" instructions above.

2. If an existing SFDC 'Prospect account already exists, Confirm that you're the "SDR Assigned". If another SDR is listed, reassign ownership to the listed. "SDR Assigned" is responsible for confirming accuracy of this field when they've been reassigned a lead.
    
3. If a SFDC `Prospect` account exists and you are the SDR Assigned, confirm that the account is associated with the lead by confirming email domain matches or that ZoomInfo associates that lead to the account. We rely on ZoomInfo as our primary source for data. In case of LinkedIn, SFDC or some other source heavily contradicting ZoomInfo, we can follow the [Sales Exception process.](handbook/sales/field-operations/gtm-resources/#sales-segment-and-hierarchy-review-process)

**Step 2**: Verify The SDR Organisation RoE

If other leads exist from the same company, but no SFDC account exists. Check `Lead Owner` and `Lead Status` fields.

1. If `Lead Status` is `MQL` for other leads too, ownership is determined by the lead whose `MQL Date` is the earlier except if `MQL Date` for another lead is more than 24 hours in the past. In this case, ownership for both leads is under your name.

2. If `Lead Status` is `Accepted` or `Qualifying`, check to see if activity has been placed in the last 14 days. If not, you can accept/work the MQL. If lead status on other lead(s) is `Qualifying`, check to see if there is a future next step in the qualification notes or last activity within the last 30 days. If so, pass the MQL to the owner of the qualifying lead. If not, you can accept/work the MQL.

3. On ZoomInfo or other sources, verify the [parent/child segmentation:](handbook/sales/field-operations/gtm-resources/rules-of-engagement/). All accounts in a hierarchy will adopt the MAX segmentation of any account in the hierarchy and ownership between segments is determined accordingly.
    
4. On ZoomInfo or other sources, verify the HQ of the company or the ultimate user. Unless [the account is `Named`](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#account-ownership-rules-of-engagement), lead ownership is determined based on the HQ of a company, regardless of lead's location geographically.

5. **For AMER/LATAM teams**, verify PubSec focus. Search ZoomInfo/LinkedIn/Website for messaging about serving the government or United States. Keywords to look for include: County, City Of, State, University, College, Defense, Intelligence, Agency, Mission, Mission Critical, Mission Support, Speed to Mission, System Integrator, Contract Vehicle, Government Bid, Government Contract, Civilian, or Task Order. Another indicator is when their list of customer accounts includes government agencies. **Canadian government leads get routed to the AMER commercial team**


**Outreach & Marketo**

If the lead is yours to work based on all of the above, sequence them in Outreach using the master sequence that correlates with the Last Interesting Moment. You can also look at the initial source and Marketo insights to get a holistic view of what a lead is looking into. There is an Outreach collection of sequences for inbound leads for each region. These collections contain a master sequence for nearly every inbound lead scenario. 
* **Master Sequence**: a sequence created by SDR leadership that should be used by all reps to follow up with inbound leads
*  [**Sequence collection**](https://support.outreach.io/hc/en-us/articles/360009145833-Collections): group of sequences compiled by region
*  **Last Interesting Moment**: data pulled in from our marketing automation software, [Marketo](/handbook/marketing/marketing-operations/marketo/), that tells you the last action a lead took.
*  [**Initial source**](/handbook/sales/field-operations/gtm-resources/#initial-source): first known action someone took when they entered our database
*  **Marketo Sales Insights (MSI)**: a section on the lead/contact in Salesforce that shows you compiled data around actions a lead/contact has taken
*  **High touch and low touch sequences**: a high touch sequence should be used for high-quality leads. High touch sequences require you to add in more personalization and have more touch points across a longer period of time, including phone calls and LinkedIn Connect requests. Low touch sequences are typically automated and run for a shorter period of time. A high quality lead has valid information for at least two of the following fields:
    * Company email domain or company name
    * Phone number
    * Title

#### Questions about a lead?
 
*  If you are unable to work out who owns the lead, Chatter your manager in the lead 
*  If you have a question about the behavior of a lead or any other operational issue, you can use the slack channel #mktgops
*  For contact requests received requesting event sponsorship, please change ownership to GitLab Evangelist in SFDC & be sure to "check the box" to send a notification.

**Technical questions from leads?**
* [Docs](https://docs.gitlab.com/)
* Slack channels
    * #questions
    * #support_self-managed
    * #support_gitlab-com

#### Dealing with spam and or unneeded leads

Every so often you may be assigned a lead that has no value to GitLab and is not worth keeping within our database. Qualities that define these types of leads include:
  * Lead info is an incoherent, jumbled combination of characters with no meaning
  * Lead info is an obvious forgery, e.g. The odds of someone being named `Batman Batman` are very low
  * Email supplied is from a temporary email domain that cannot be contacted after a certain time limit due to email self-termination

When you come across leads that fall into this category, it is best to remove them from our Salesforce database entirely. The process to remove leads is to add the lead to the [SPAM DELETION Salesforce campaign](https://gitlab.my.salesforce.com/7014M000001dphz). A few details about this process:
  * Only add leads to this campaign if you are 100% certain they are worth removing from the database
  * MktgOps cannot recover leads removed through this process. You should consider the removal permanent, so be careful
  * The `campaign member status` used when adding to the campaign does not matter, only `campaign membership` matters for this process
  * The removal process, which is completed via `Marketo`, runs once a day at 12:05am PDT
  * If you require leads be cleared out before the scheduled time, please ask in the `mktgops` Slack channel. Please keep unscheduled requests to a minimum
  * If you spot a number of leads with the same domain, please [create an issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new) with Marketing Ops to have that email domain added to a block list. You can still follow steps above to delete them.

### Qualification Criteria and SAOs

Qualification criteria is a minimum set of characteristics that a lead must have in order to be passed to sales and become a Sales Accepted Opportunity (SAO). You will work to connect with leads that you get a response from to obtain this information while assisting them with questions or walking them through how GitLab might be able to help with their current needs. The qualification criteria listed in the linked handbook page below aligns to the 'Qualification Questions' sections on the LEAD, CONTACT, and OPPORTUNITY object in Salesforce. In order to obtain an SAO you will need to have the 'required' information filled out on the opportunity including documented 2-way communication on the Contacts in the Opportunity.  
*  [Qualification criteria needed](/handbook/sales/field-operations/gtm-resources/#criteria-for-sales-accepted-opportunity-sao)

When a lead meets the qualification criteria and you have an IQM/intro call scheduled with your AE/SAL, you will convert the lead to an opportunity. 
* [ How to create an opportunity](/handbook/sales/field-operations/gtm-resources/#creating-a-new-business-opportunity-from-lead-record)

### Working with 3rd Parties (resellers, partners, consulting firms, and/or companies that buy GitLab for their customers)
**The end user account determines the SDR alignment:** If you are the SDR assigned to the 3rd party lead, please gather the information in point 1 and pass the lead to the correctly assigned SDR (the SDR assigned to the end user account) who will complete points 2-6 

1.  Gather billing and end user details from the reseller:
    * Billing company name/address:
    * Billing company contact/email address:
    * End user company name/address:
    * End user contact/email address:
    * [Snippet in outreach](https://app1a.outreach.io/snippets/362)
2. Create a new lead record with end user details 
    *  Ensure that all notes are copied over to new LEAD as this is the LEAD that will be converted.
3. Converting the new lead
    * Name opp to reflect reseller involvement as shown here: “End user account name via reseller account name”
4. Convert original reseller lead to a contact associated with the reseller account
    *  If an account does not already exist for the reseller, create one when converting the lead to a contact.
    *  Assign record to the same account owner
    *  Do NOT create a new opportunity with this lead.
5. Attach activity to the opportunity
    * On the reseller contact, go to the activity and link each activity related to your opportunity to the opp.
        * Activity History > click edit to the left of the activity > choose 'opportunity' from the 'related to' dropdown > find the new opportunity > save
6. Update the opportunity
    * Change the business type to new business and stage to pending acceptance.
    * Under contacts, add the reseller contact, role as reseller, and primary contact.
    * Under partners, add the reseller account as VAR/Reseller"

### Drift Chat Platform
We use a the chat platform Drift to engage site visitors. As an SDR, you are expected to be active on Drift throughout your work day. The [tool's handbook page](/handbook/marketing/marketing-operations/drift/) will walk you through guidelines, best practices and troubleshooting. 

### Working with the Community Relations Team
The [Community Relations team](/handbook/marketing/community-relations/) owns GitLab's [Education](/handbook/marketing/community-relations/education-program/), [Open Source](/handbook/marketing/community-relations/opensource-program/) and [Startups](/solutions/startups/) programs. When a lead fills out the form to apply for one of these free community programs, Salesforce `Lead` ownership will automatically change to the `Community Advocate Queue`. If this Lead was in an Outreach sequence, it will automatically be marked as finished. 

The [Community Operations team](/handbook/marketing/community-relations/community-operations/) (a sub-team of Community Relations), and the Program Managers for each program, will then work to qualify the lead. If the Lead does not end up qualifying for one of the programs, they will be passed straight to sales.
- Forms
     -   [GitLab for education](https://about.gitlab.com/solutions/education/)
     -   [GitLab for Startups](https://about.gitlab.com/solutions/startups/) 
     -   [GitLab for Open Source](https://about.gitlab.com/solutions/open-source/)
- Questions
     - Chatter the relevant Program Manager in Salesforce. For EDU: `@Christina Hupy` and for OSS and Startups: `@Nuritzi Sanchez`.
     - Slack channel: `#communnity-programs`

## Outbound/Inbound Prospecting 

### Outbound Prospecting

Outbound lead generation is done through prospecting to companies and individuals who could be a great fit for our product. Prospecting is the process of finding and developing new business through searching for potential customers with the end goal of moving these people through the sales funnel until they eventually convert into customers. 

You will work closely with your dedicated SAL or AE to build a strategy for certain companies; or parts of your territory or personas you as an aligned team want to focus on. It is crucial that you are very intentional and strategic in your approach and always keep the customer experience in mind. When reaching out, offer value and become a trusted advisor to ensure we always leave a positive impression whether there is current demand or not.

You can focus on Closed Lost Opportunities once they are older than 30 days, in line with the Sales Handbook [Opportunity Stages](/handbook/sales/field-operations/gtm-resources/#opportunity-stages)  
You can focus on Leads 60 days after the date of their last hand-raising activity if you inherit a geo/region [Active vs. Passive](/handbook/sales/field-operations/gtm-resources/#active-vs-passive)

### Cold-Calling Checklist 

Our cold-calling best practices typically consist of 4 elements. A pattern interrupt and an elevator pitch and, if required, objection/trap-setting questions and Up-Front Contracts. 

To be effective, these need to be customized to the individuals we call as per the logic below:
* Different Geographical Business Cultures
* Personality Type as per the DISC model
* Individual's Role and Responsibilities
* Business’ Needs and Objectives

The main documentation may be found [here](https://docs.google.com/document/d/1D3iV_WW5fRidRN5H8-3SZVAAr3ffEvjxUC6cW5SFXDM/edit) and the library of resources [here](https://drive.google.com/drive/u/0/folders/1VhxVwy4DoEvFco_wXeOLb0DLtVIfo7wQ)

### Up-Front Contract

Typically used for scheduled calls, the Up-Front Contracts or UFCs can also be used as a defense mechanism when cold calling does not go as planned. For cold-calling, UFCs are used when the prospect can commit to 15’ at the time of the call. If they commit to a 15’ for a later time, UFCs are used to open the call and set the agenda then. 

Explore the main documentation [here](https://docs.google.com/document/d/1Y7qEq8g3eHh5-oagERGvNmatiOV3JXi9Tw46SKWwpNM/edit) and the library of resources may be found [here](https://docs.google.com/document/d/1Y7qEq8g3eHh5-oagERGvNmatiOV3JXi9Tw46SKWwpNM/edit) 

### Email Wrting Cheat Sheet

The table below shows the Command of the Message Email Writing Checklist.

It aims to outline the structure of the emails we write to prospects. Emails should be written in each person’s language and tone and make sure to include the CoM frameworks as per the outline below. You can find additional resources [here](https://docs.google.com/document/d/1-DF6bEtS9QF9idqBcK77RiLL04CKiFMuc0LDEM5N6RA/edit)


| Subject Line                                  |                                                                                                                                                                                                                                                                                                      |
|-----------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Personalized                                  | Quote Familiar Names: Use Prospect’s name, Company name, or colleague’s name. Relate to their situation: Use recent company initiatives, technologies they use, Projects they have planned etc.                                                                                                      |
| Curiosity and Urgency                         | Provide image of future situation: Guide the reader to imagine how their situation could change. Use compelling events to give a clear image of where they are currently.                                                                                                                            |
| CoM                                           | Proof Points: Quote Case Studies or other customers. How We Do it Better: Make use of our defensible differentiators.                                                                                                                                                                                |
| Opening Line                                  |                                                                                                                                                                                                                                                                                                      |
| Avoid cliches                                 | Stand out: Avoid losing email real-estate with cliche phrases like “just following up” or “hope all is well”.  Brand yourself: Demonstrate that you have understanding of the reader’s current situation and clear idea of his required solutions.                                                   |
| CoM                                           | Before Scenarios: Paint a clear image of their current situation and how that is causing them professional or personal pain. Ater Scenarios/PBOs: Tie the current situation with a clear image of a future situation that’s beneficial for the business and the individual.                          |
| Main Body                                     |                                                                                                                                                                                                                                                                                                      |
| Addressing Questions and Information provided | No Free Consulting: Answer questions just enough to give a narrative to your email and tie into the CTA. No Transactional Answers: Don’t make the reader feel like he’s interacting with an online form, a robot or a sleazy salesman that doesn’t care.                                             |
| CoM                                           | Discovery Questions: determine which discovery questions tie into their current situation, questions asked or information provided. Trap-Setting Questions: if competitor technology or objections come up, use trap-setting questions to guide the reader into understanding our differentiators.   |
| CTA                                           |                                                                                                                                                                                                                                                                                                      |
| Clear Next Step, Agenda and benefit           | Valuable: phrase your CTA in a way that’s clearly valuable to the reader and his specific situation. Defined: outline what will happen in the next step and how long will it take                                                                                                                    |
| CoM                                           | Measurable: present metrics or PBOs that will be covered in the next step                                                                                                                                                                                                                            |

### Focus Accounts

**Focus account**: an account in which you as an SDR have developed an account plan with your AE or SAL and are actively focusing on via outbound prospecting.

Focus Accounts should be kept up-to-date in Salesforce in real-time, the SDR Target Account box checked as soon as you begin the account planning process. If the account does not have activity associated with leads/contacts over a 30-day period, the account should no longer be focused on and the SDR Target Account checkbox should be unchecked. 

If you check the targeted checkbox, you are committed to:
* Developing an account plan with your AE or SAL
* Actively outbounding to influencer and/or user personas within the account

### Database Segmentation
The SFDC prospect database is split into five segments: Core users, free GitLab.com users, trial prospects, current customer "leads" and other prospects. We have checkboxes for these, most of which will automatically populate. **However**, the `Core User` checkbox in Leads and Contacts needs to be checked by the SDR or Sales rep once the prospect has confirmed *in writing or on the phone* that they are using Core as we cannot rely on our product analytics alone as it does not cover all users. 

### Outbound Workflow
**IMPORTANT**: EMEA reps, old leads/contacts for EU nations can only be called or emailed if they were in our system from May 2018 or later. Any lead/contact that was brought into our system prior would need to opt in to communication. Any new leads/contacts can only be called or emailed if they have been brought in from ZoomInfo. You can also leverage LinkedIn Sales Navigator. 

The [SDR outbound framework](https://drive.google.com/drive/search?q=%20Outbound%20Process%20Framework%22%40gitlab.com) (sample is linked) will walk you through both strategy and execution of outbound prospecting.

When creating new prospects in SFDC these should be added as Leads to the system. New Leads can for example be prospects you are talking to on LinkedIn, prospects you are introduced to by somneone at GitLab or by other Leads or Contacts in SFDC. 
Everyone you are contacting should exist on SFDC so that you, your manager, and the Account owner have a full picture of who you are prospecting into. LinkedIn conversations can be "Copy to CRM" within the LinkedIn Sales Navigator chat platform. 

If you have a list you would like to import to SFDC please follow this [List Import](https://about.gitlab.com/handbook/marketing/marketing-operations/list-import/) process on our Marketing Operations page which outlines how to import CSVs and ZoomInfo Prospects into the system. 

### Outbound Messaging

[**Outreach Sequence Library**](https://drive.google.com/drive/u/0/search?q=%22sdr%20outreach%20sequence%20library%22)

**Persona-based Messaging:** *Used for focusing on specific titles or personas*
 - [VP, App Dev (Erin)](https://app1a.outreach.io/sequences/6397)
 - [Director, App Dev (Dakota)](https://app1a.outreach.io/sequences/6392)
 - [Manager, App Dev (Alex)](https://app1a.outreach.io/sequences/5983)
 - [CISO (Skyler)](https://app1a.outreach.io/sequences/6426)
 - [Snippets](https://app1a.outreach.io/snippets?direction=asc&order=owner&content_category_id%5B%5D=18)

**Business Resilience Messaging** *Revamped value driver messaging for companies impacted by the pandemic*
 - [Deliver Better Products Faster](https://app1a.outreach.io/sequences/5665)
 - [Increase Operational Efficiencies](https://app1a.outreach.io/sequences/5678)
 - [Reduce Security & Compliance Risk](https://app1a.outreach.io/sequences/5746)
 
**Value Driver Messaging:** *Developed to align to the [GitLab value framework](https://about.gitlab.com/handbook/sales/command-of-the-message/#customer-value-drivers)*
 - [Deliver Better Products Faster](https://app1a.outreach.io/sequences/5815)
 - [Increase Operational Efficiencies](https://app1a.outreach.io/sequences/5816)
 - [Reduce Security & Compliance Risk](https://app1a.outreach.io/sequences/5817)


## Event Promotion and Follow Up Assistance

The SDR team will assist the Field Marketing and Corporate Marketing teams with campaign and event promotion and follow up. Learn more about [Field](/handbook/marketing/revenue-marketing/field-marketing/) or [Corporate Marketing](/handbook/marketing/corporate-marketing/) via their linked handbook pages. 
When these teams begin preparing for an event, they will create an issue using an issue template. Within that template is a section titled ‘Outreach and Follow Up’ for the SDR team to understand what is needed from them for each event. 
*  [Field Event Template](https://gitlab.com/gitlab-com/marketing/field-marketing/tree/master/.gitlab/issue_templates)
*  [Corporate Event SDR Support Template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/Event-SDR-Support-Template.md)

#### Field Process

When our Field Marketing Managers (FMMs) begin preparing for an event or micro campaign, they will create an issue using a [regional issue template](https://gitlab.com/gitlab-com/marketing/field-marketing/tree/master/.gitlab/issue_templates). If they need SDR Support they will open an [SDR Request issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=ISR_SDR_FMTemplate) and the FMM will loop in the appropriate SDR manager(s) on this issue as well as fill out/ensure the following sections are accurate:
* Pre-event/campaign goals
* Post-event/campaign goals
* Targets (title and number of attendees) 

Once the sections are populated and up to date, the SDR manager is looped in. They will then elect an SDR with bandwidth to begin completing the steps in each of the above sections. The SDR will: 
* Read the issue to get an understanding of the event
* Complete the tasks under the ‘Pre-event/Post-event’ section based on the deadlines next to each bullet. 
* Slack their manager and/or the FMM if they have questions. 

#### Corporate Process
The corporate process is the same as the field process detailed above with the exception that they have a separate issue when looping in the SDR team for assistance that will link to the larger event issue. 

#### Event Deadlines
**Event Promotion**

*  The SDR manager needs to be assigned a minimum of 30-days prior to the event, but the sooner they can start prepping, the better.
*  The SDR manager(s) should have their teams begin compiling lists as soon as possible. Focus lists need to be completed three weeks prior to the event so that we will have three weeks of promotion
*  The SDR project lead needs to have their focus account list as well as the shared sequence completed, reviewed and approved at least three weeks prior to event so that we will have three weeks of promotion

**Event Follow Up**

*  SDR project lead needs to have the follow up sequence created and reviewed with management and FMM one week prior to event. The copy doc for the event is linked in the event Epic and can be used for reference. 
*  FMM will ensure leads from Field Marketing initiatives will be routed to the SDR within 48 hours.

**Sequence Information**
- Please clone one of the Master Event Sequences found in the [Events Outreach collection](https://app1a.outreach.io/sequences?direction=desc&order=recent&content_category_id%5B%5D=6) and pre populate as many of the variables as possible. If all of the variables can be populated manually or through Outreach/Salesforce, change the email type from ‘manual’ to ‘auto.’ Work with your manager and the FMM to make any other additional adjustments that may be needed.

#### Outbound Campaign Process

[Process Overview](https://www.youtube.com/watch?v=b-5cQX3-aZ0)

SDRs have found particular success when organizing campaigns alongside regional Field Marketing Managers to ensure repeatable tactics and measurable results. The workflow below outlines what that process would entail should it be determined that you leverage this motion. 

**Process Steps**

**Step 1:** Speak with your SDR Manager to identify the Field Marketing Manager (FMM) assigned to your SAL’s & AE's/Territory/Targeted Accounts. Set meeting with FMM to discuss existing Campaigns in motion, your ideas for Campaigns, and previously successful Campaigns (15 minute meeting over Zoom).
[Example of a Successful Campaign](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/1750)

**Step 2:** Once you've identified a Campaign to launch, working closely with your FMM (FMM will create an Issue in GitLab related to the campaign), and go into Salesforce Reporting to generate a targeted Contact list for the campaign.

**Workflow for creating a Salesforce report** (Screenshots below):
Go to Reports -> Create New -> Make a Report under Contacts & Accounts -> Add Field Filter Logic (SDR Assigned equals SDR Name, Contact Status does not contain Unqualified & Title Contains: Software Development, DevOps, Engineering, Security, Operations) -> Custom Date Range = from: last two years up to: the last two weeks and type in Last Interesting Moment into the field section and drag/drop the field into the Column Preview.-> Tip: - Organize your fields on the report to make it easier to read (First Name, Last Name, Account Name, Title, LIM, State, Country, Phone, Mobile, Email, Account Owner -> Run Report.

Please note, you can use this same Report workflow for generating a Lead Report. Simply select Leads from the New Report Options. Before you run the Report, please make sure that the Report Options -> Show is showing All Contacts/All Leads not My Contacts/My Leads by clicking the drop down and selecting All Contacts/Leads.

Save your Report by clicking the Save As tab. Make sure to Save Report under Sales Development Representatives folder and add a Unique Report Name (see example)

Add Campaign name from SFDC Campaign that is listed in the Field Marketing SDR specific Issue. Example: 20200909_TShirtSurveyOutreachCa -> Member Status = Member -> Click Add.

Your Report for your FMM Campaign is now complete! Keep your Unique Report Name in mind for when you upload Report to Outreach (see Step 4).

**Step 3:** It's time to generate content for your Campaign in Outreach. Note, if the Campaign previously ran, use existing content (FMM will confirm). Otherwise, work with your FMM and SDR Manager to delegate the resources you need to create content for the Campaign.
[Outreach Campaign Example](https://app1a.outreach.io/sequences/5957?accounts-nonce=5583950dda94071d35e1)

**Step 4:** Bulk add Contacts or Leads pulled from Step 2 to the Campaign in Outreach (ensure Campaign content is revised and reviewed by SDR Manager and FMM).
**Workflow for Bulk Creating a SFDC Report in Outreach for Sequencing:** 
In your Outreach 360 view, in the top left corner select Actions -> Select Bulk Create -> Select SFDC Report and type in your Unique Report Name 

Select Next, then re-map any missing fields. Once Report is Uploaded and you are in Prospect View, select the Prospects you want to Add to Sequence and then add to the Campaign Sequence. You have now uploaded your Target Contact or Lead List SFDC Report into Outreach and Sequenced List into Campaign for Outbound prospecting!

**Step 5:** After Contacts or Leads are loaded into the appropriate Campaign Sequence, alert your Manager and FMM in the appropriate issue for review before activating (FMM approval recommended to start Campaign in Outreach).

**Step 6:** Check the Campaign daily in Outreach throughout duration to ensure all tasks are completed. Provide updates in the Issue created in GitLab by the FMM related to the campaign (updates can include IQM conversion rate, Open rates, IQM to SAO conversion rates, etc.)

**Step 7:** Upon commencement of the Campaign, post a final update in the Issue that includes your results (updates can include number of IQMs, Open rates, IQM to SAO conversion rates, etc.)

**Step 8:** Recommended to schedule 15 minute meeting with FMMs to recap Campaign and provide feedback (this meeting can focus on final results, and if Campaign should stop, continue, or be revisited in the future).

Optional Post Event Follow Up: Continue to monitor Contacts and Leads uploaded into the Campaign for lingering results (there are sometimes stragglers on Campaigns that respond after the Campaign has concluded). An easy way to monitor is setting one or two tasks in Outreach for 2 weeks and 4 weeks after the Campaign has concluded.

## Account Based Marketing: SDR Workflow & Best Practices
Account-based marketing (ABM) is a marketing strategy in which a company identifies key accounts that they want to pursue. Unlike traditional marketing efforts, instead of focusing on an individual, an ABM strategy focuses on an entire company, treating each account as its own market.

The SDR team is a CRITICAL part of account-based marketing. Without SDR's hard work and account knowledge, ABM efforts would stall. In an Account-Based Marketing world, the total number of SDR activities (calls/e-mails) may decrease, but the relevance and personalization of interactions increases significantly. With SDR customization and careful prospect handling mixed with ABM marketing efforts, we will be able to land and expand our account list.

Learn more about ABM at GitLab by going to the [Account Based Marketing handbook](/handbook/marketing/revenue-marketing/account-based-strategy/account-based-marketing/) page!

#### ABM Outreach Sequences 
The ABM team has created three Outreach Sequence templates based on our research and work with consultants that can be found in the ABM folder in Outreach. These templates are meant to be cloned to ensure settings and steps carry over. Once cloned, feel free to use the content from other sequences to populate your cloned sequence but please ensure you leave manual step types for additional personalization. The main priority is having the correct steps and number of steps that align with the template use case. The three templates are as follows: 
- Inbound
- Outbound
- Re-engage

#### Account Based Account Planning 

When working with an ABM account, SDRs are expected to thoroughly research these accounts and develop a strategy. The following topics should be researched and documented in Salesforce for each focus account:

**Uncover general account information**
* How do they refer to themselves and their teams? (review website, press releases, company content / articles, org chart)
* Look at current IT job postings (indeed / create alerts)
* Research recent news / headlines (google search / create alerts)
* Understand organizational structure (ZoomInfo org charts, general research)
* Look at financials, earnings reports, 10ks (ZoomInfo, Google search, general research)
* Identify key investment / development areas. (Are they focused on website, mobile app, user experience, migration/cloud initiative, security issues / breaches, cost cutting initiatives, strategic projects, etc.?)

**Review history with GitLab**
* Capture actionable intelligence from the SAL or account owner
* Review all Closed/Lost opportunities 
* Understand current CE usage / GitLab footprint (using version.gitlab.com)
* Look at recent leads, engagement, website activity (using Salesforce and DemandBase)

**Understand intent and buying signals**
* Intent determination (using Salesforce reports plus the weekly update via Demandbase)
* Live intent (via Slack alerts)
* Create job alerts (via indeed / Google)

**Discover the right prospects**
* Import ALL key IT prospects from ZoomInfo to Salesforce ("VIP", "Influencers", "Users", "Change agents", "Fans", and "Potential Users")
* Identify other key prospects not in ZoomInfo using LinkedIn Sales Navigator
* Source additional email- and phone- validated prospects on an as needed basis

**Know your vertical(s)**
* Thoroughly review the account based marketing campaign briefs
* Review relevant case studies (GitLab and non-GitLab)
* Research industry thought leaders / events / industry hot topics
* Understand the competitive landscape (Crayon / research)

**Work with Sales to determine and documented outreach strategy**
* Discuss recent interactions
* SALs share actionable intelligence on the account
* Determine top focus titles and personas
* Document outbound plays and outreach strategy

Note: It is the responsibility of the SDR to coordinate and document outbound plays, tactics, and to ensure that all communication with accounts and prospects is logged or documented. 

#### Account Based Outbound Workflow
If you have an ABM account, you will need to follow [this](https://docs.google.com/document/d/1ZwP23cIwZ692XcO5MsJl2611wQAjsnNn9J1ZSrfdZVY/edit?usp=sharing) outbound workflow to ensure we are properly measuring and tracking all outbound efforts. There is also an [unfiltered YouTube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrrAseaD0m4ty4oPU8FyKGD) that walks you through the tooling workflow step by step.

#### SDR ZoomInfo Prospect Discovery
Discovering new prospects is an important part of ABM account development. In order to do this in an efficient and organized manner, we recommend using the Focus Persona Segments and process listed below.

**Focus Segments**

* **VIP**: *CTO, CIO, CSO, CISO, VP of IT, Managing director of IT, Chief Architect, Chief Systems architect, Global Head of DevOps, Head of DevOps, Head of Engineering, Head of Development*
* **Influencer**: *Directors or Managers of software development, software dev, software engineering, engineering, application development, app dev, devops, security, site reliability, site reliability engineer, cloud engineer, cloud architect, cloud services, cloud native app dev, cloud native application development, enterprise architect, principal architect, software architect, data architect, technical architect, application architect, architect, consultant*
* **User**: *Developers, engineers, Q/A, test, business users, interns/students*

`Please note: these should be imported as LEADS, not contacts`

Here is a video that shows you [how to install the ZoomInfo integration and use it to quickly export leads to SFDC](https://www.youtube.com/watch?v=67x85B8mz1g).

## SDR Team Overview


| Account Segment | Region | Type  | Manager | Routing Directions | 
| ------ | ------ | ------ | ------ | ------ | 
| Large | Amer East | First Order/Land | Maggie Barnes | If the account has “First Order Available” checked + ultimate parent is not a customer + there is a SDR assigned, route to that SDR. If there is no SDR assigned, chatter manager to route. |
| Large | Amer West | First Order/Land | Maggie Barnes (interim) | If the account has “First Order Available” checked + ultimate parent is not a customer + there is a SDR assigned, route to that SDR. If there is no SDR assigned, chatter manager to route. |
| Large | Emea | First Order/Land | Miguel Nunes | If the account has “First Order Available” checked + ultimate parent is not a customer + there is a SDR assigned, route to that SDR. If there is no SDR assigned, chatter manager to route. |
| Large | Amer | Expand | Hannah Schuler | If "First Order Available box is unchecked and ultimate parent is based in Amer, the SDR assigned should be on the account. If it is not, leverage the grid [here](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#amer-enterprise-land-alignment), and route to the SDR that is aligned to the account executive. If it is still unclear, chatter manager |
| Large | Emea | Expand | Hailey Pobanz | If "First Order Available box is unchecked and ultimate parent is based in Emea, the SDR assigned should be on the account. If it is not, leverage the grid [here](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#emea-enterprise-land-alignment), and route to the SDR that is aligned to the account executive. If it is still unclear, chatter manager |
| Midmarket | Amer | Net New | Sunanda Nair | Leverage [this Excel sheet](https://docs.google.com/spreadsheets/d/1tnMIH63t3xCj9bzFfGLFQXxjre-kiiNw3wO37GRYnfQ/edit#gid=0). >500 employee accounts route to specific SDR, all other accounts route by state AE. If state/AE can't be determined, chatter manager. |
| Midmarket | Emea | Net New | Panos Rodopoulos | Round Robin on inbound leads - you will need to chatter manager
| SMB | Amer | New New | Ramona Elliott | SMB in US and Canada is round robined. Will need to leverage [this report](https://gitlab.my.salesforce.com/00O4M000004oPaE) to determine who has the least MQL's and is up next to receive. Latam is routed to Bruno Lazzarin.
| SMB | Emea | Net New | Panos Rodopoulos | Will need to chatter manager.
| SMB,MM,Large  | Apac | All | Glenn Perez | Routing can be found [here](https://docs.google.com/spreadsheets/d/1DuWjG89QNR-0TmPAUNoTenODtt4Cv9MzoWABZ6wQgO0/edit#gid=0), if it is still unclear, chatter manager
| PubSec | Amer | Net New | Ramona Elliott | Routing can be found [here](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#pubsec-amer-sdrs) Canada gov leads route to AMER commerical team

# PubSec AMER SDRs
We currently have 3 SDR's focused on supporting the PubSec ISR's in the US. Their alignment is as follows:

| PubSec SDR | Group | 
| ------ | ------ |
| Evan Mathis| National Security Group (NSG 3-6) |
| Evan Mathis | Civilian 3 & 5 | 
| Evan Mathis | State, Local, Edu (SLED) | 
| Whitney Hurston | Civilian 2, 6 & 7 | 
| Whitney Hurston | Department of Defense (Army/Navy/Airforce |
| Josh Downey | DoD Agencies and National Security Group (NSG 1,2)  | 
| Josh Downey | Systems Integrators (SI) | 

PubSec SDRs have a different workflow from other SDRs in that they do not convert leads into opportunities. They input qualification notes on the lead record after their intro call and then set up the IQM with the ISR. The ISR converts the lead into the opportunity to ensure the opp is assigned to the correct account. Once the ISR has determined our SAO criteria has been met, they are responsible for putting the SDR on the opportunity and moving the opportunity into discovery. 

The PubSec SDRs will work closely with PubSec FMM's and MPM's on pre and post event outreach. For events that are limited in attendance capacity, the PubSec ISRs will own pre and post event strategy, not the SDR's. 

[SDR PubSec Issue Board](//gitlab.com/groups/gitlab-com/-/boards/1864446?label_name[]=SDR%20Pub%20Sec) - used to track relevant GitLab issues involving the PubSec SDR team. This is a global issue board and will capture all issues in any group/sub-group in the GitLab.com repo when any of the following scoped labels are used.


- `SDR Pub Sec`- Issues concerning the PubSec SDR team. When PubSec FMM's/MPM's add this tag to an event, these issues will appear on our board. When applicable, the PubSec SDR manager will create a SDR follow up issue and relate it to the event issue so that marketing members can easily track our work. 
- `SDR Pub Sec:: FYI` - Needs attention.
- `SDR Pub Sec:: In Progress` - Related work needed is in progress.
- `SDR Pub Sec:: Completed` - Related work is completed and ready for SDR manager to review/close.
- `SDR Pub Sec:: Revisiting` - Campaign is being revisited and additional outreach is being done.
- `SDR Pub Sec:: Evan FYI` - Issues pertaining specifically to Evan.
- `SDR Pub Sec:: Josh FYI` - Issues pertaining specifically to Josh. 


# Global Enterprise Land SDR Team 
The Enterprise Land SDR team is responsible for creating qualified meetings and sales accepted opportunities with net new customers in the Large - Enterprise Segment. The team is composed of SDRs who focus on account-centric, persona-based outreach, strategic outbound prospecting, social selling and creative outreach to educate and create internal champions that align with the GitLab value drivers and use cases.

## AMER Enterprise Land Alignment

East Land
- Marcus Stangl
- Jesse Muehlbauer
- Chelsey Maki
- Eduardo Guillen
- MaKayla Emahiser
- Michael Lerner
- Micah Lockerby

West Land
- Matthew Macfarlane
- Blake Chalfant-Kero
- Paul Oakley

LATAM Enterprise
- Leo Vieira

These SDRs are aligned by accounts - see Salesforce report https://gitlab.my.salesforce.com/00O4M000004aftz.

If you are trying to route a lead and the account is First Order Available and does NOT have an SDR Assigned on the account, please chatter Maggie Barnes and she will assign to the SDR via round robin routing. 

## EMEA Enterprise Land Alignment

| **Role**         |  **SDR**             |  **Lead & Account Assignments**      | 
| :------------- |  :--------------------------------- | :----------------- | 
| Northern Europe - Land       |  Kalyan Nathadwarawala   |  UKI   | 
| NE & MEA - Land       |  Open Role   |  Benelux, Middle East, South AFrica  | 
| NE & CEE - Land       |  Eltaj Muradli   |  Nordics, Central & Eastern Europe  | 
| Southern Europe - Land       |  Remi Lacaze   | France, Emerging Markets (Spain, Italy, Portugal)  | 
| DACH - Land       |  Lidia Murasan   | DACH  | 

## Expectations 

**Land SDR**

Named SDRs are responsible for:
* Leading account planning efforts and creating in-depth account plans for top focus accounts
* Qualifying and converting marketing-generated leads (MQLs) and inquiries into sales accepted opportunities (SAOs)
* Aligning prospecting efforts to the field, corporate, strategic and account based marketing campaigns
* Educating prospects and making positive impressions
* Generating awareness, meetings, and pipeline for sales
* Collaborating with peers, marketing, and sales teams

Requirements:
*  Meet monthly quota
*  Maintain a high sense of autonomy to focus on what's most important
*  Participate and lead planning, execution, and cross-functional meetings 
*  Participate in initial qualifying meetings, discovery calls, and follow-up conversations
*  Maintain exceptional Salesforce hygiene, logging all prospecting activity, conversations, and account intelligence uncovered
*  Generate IACV Pipeline

#### Working with Sales
SDRs should be in regular communication with the SALs that they support. When taking on new accounts or supporting a new SAL, it is the responsibility of the  SDR to set up a kickoff call to:
1. Review the focus accounts
2. Gather actionable intelligence from SALs/AEs
3. Review outbound SDR plays, campaigns, field tactics, and other offers
4. Document the plan using docs, a spreadsheet, or the SDR quarterly issue plan 

Recurring SAL<>SDR meetings can be scheduled or take place on an as-needed basis. As SDRs are not directly aligned with SAL we encourage async updates as much as possible

Slack is encouraged for informal communication, but anything related to strategy, approach, or outreach should be documented.


### Geo SDR Account Prioritization Model

**Account Scoring & Prioritization**

The next step after identifying all of the Net New focus accounts in a region is to prioritize them. The scoring model below should be used to determine the Net New Account Tier which will help guide prioritization and prospecting efforts using the Outbound Prospecting Framework (below).

`A Tier Accounts` - Have at least 4 out of 5 qualifiers below

`B Tier Accounts` - Have 3 out of 5 qualifiers below

`C Tier Accounts` - Have 2 out of 5 qualifiers below

`D Tier Accounts` - Have 1 out of 5 qualifiers below

`F Tier Accounts` - Have 0 out of 5 qualifiers OR zero direct revenue potential in the next 3 years

**Account Scoring Qualifiers:**
*  Current CE Usage
*  250+ employees in IT/TEDD positions
*  Good Fit Industry / Vertical (High Growth, Technology, Financial, Healthcare, Regulated Business)
*  Early Adopters / Innovative IT Shops (Identifiers & Keywords): Kubernetes / Containers, Microservices, Multi-cloud, DevOps, DevSecOps, CICD (and open-source + proprietary tools), SAST / DAST, Digital Transformation
*  Current DevOps Adoption (multiple DevOps roles on staff or hiring for multiple DevOps positions)

**Outbound Prospecting Framework**

| **Tier**        |  **Goal**                 | **Priority Level**      | **Outbound Tactics**      |
| :---------- |  :----------                  | :----------------- | :----------------------|
| A Accounts   |    Conversations, IQMs, MQLs         |      High (60% of focus)      | Hyper-personalized, simultaneous outreach, creative, direct mail, ads, groundswell, events |
| B Accounts   |  Conversations, IQMs, MQLs           |      High (30% of focus)      | Hyper-personalized, simultaneous outreach, creative, direct mail, ads, groundswell |
| C & D Accounts |  Conversations, MQLs               | Low  (< 10% of focus)         | Automated role- / persona-based outreach, groundswell |
| F Accounts |  Eliminate from SDR focus account lists   | Low (< 2% of focus)     | Do not focus on or attempt to qualify |


## Helpful definitions

**Hyper-personalization** - This is the concept of combining real-time data extracted from multiple sources to create outreach that resonates with prospects on an individual level. The desired outcome is to establish relevance with a prospect as a first step towards a conversation.

**VIP (prospect)** - A Very important top officer, executive buyer, C-level prospect, or important influencer. For these individuals, hyper-personalization is required. Examples: CTO, CIO, CSIO, C-level, IT Business unit leads, VPs, strategic project leaders.

**Influencer (prospect)** - An individual prospect that is suspected to be involved with IT decision-making, tooling, teams, roadmap, strategic projects, and/or budgets. Examples: Director or Manager of DevOps / Engineering / Cloud / Security, Enterprise Architects, IT buyers, SysAdmins, purchasing managers, and product owners.

**User (prospect)** - A prospect that has limited influence within an IT organization. Examples: Developers, Engineers, QA, consultants, and business users.

**Groundswell** - An outbound strategy focused on filling the top of the funnel by generating engagement, opt-ins, MQLs, and uncovering intent signals. This strategy typically incorporates more automation than other more direct outbound prospecting tactics. The strategy should be used with lower-level prospects and lower tier accounts.

**Snippets** - Content created for SDRs to use to create hyper-personalized sequences, one-off emails, or to use for reaching out to prospects via LinkedIn. 

**Warm Calling** - The method used to strategically incorporate phone calls and voicemails into an outbound prospecting workflow. The idea is optimize outbound productivity by only using the phone to call *engaged*, *validated*, and/or *VIP* prospects. 

# Global Enterprise Expand SDR Team
The Global Enterprise Expand SDR team is responsible for creating qualified meetings, sales accepted opportunities and pipeline within existing customer accounts (Accounts with total CARR in parent/children/subsidiaries in a hierarchy) in the Large - Enterprise Segment.   
The team is composed of SDRs who focus on strategic account-based, persona-based outreach, strategic outbound prospecting, social selling and creative outreach to create and expand internal champions that align with the [GitLab value drivers](https://about.gitlab.com/handbook/sales/command-of-the-message/#customer-value-drivers) and use cases.   
The aim for the SDR is to find new business units within these customer account hierarchies who are looking to move from a competitor/GitLab Core/CE to a paid version of GitLab.  
SALs alone are responsible for renewing/uptiering existing paid customer instances to a higher paid version of the product, eg from Standard to Premium. 

## How GitLab defines a customer 

Account alignment is based on the ultimate parent. If any account within the ultimate parent hierarchy is a customer and has a `Total CARR` amount of $48 or over, then the entire hierarchy is considered customer and worked by the Expand team.  

Total CARR of $48 and below is deemed as a Land Account and these accounts are flagged with a check mark in the `First Order Available` field on the account object. 

Accounts with no check in the box fall to the Expand team.


# Global Enterprise Expand Alignment

AMER Enterprise SDR Expand Team is aligned to account territories in US West and US East regions. Each region has verified Growth scores/CARR per territory that help determine equitable spread of SDR alignments as shown below. Target accounts are based on SAL strategy, potential CARR, Intent data (propensity to buy), and industry.  

EMEA Enterprise SDR Expand Team is aligned to account territories in Northern Europe, DACH, France/Southern Europe, and emerging markets. Target accounts are based on SAL strategy, potential CARR, Intent data (propensity to buy), and industry.

| **Role**                    | **SDR**                | **Territory Alignment**                                                                                                                                                                                             | **Strategic Account Leader**                                                   |
|-------------------------|--------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------|
| AMER - East - Expand    | Kaleb Hill         | ENT-SE-Named 2, ENT-CE-Named Chicago, Large-AMER-Southeast-1, Large-AMER-Central-3, Large-AMER-Central-2, ENT-SE-Named 1,   Large-AMER-Central-3                                                                                                                                    | Josh Rector, Chris Graham, Matt Petrovick, Brandon Greenwell, Chip Diglioramo             |
| AMER - East - Expand    | Ryan Kimball       | Large-AMER-Southeast-2, Large-AMER-Central-4, ENT-NE-Named NYC, ENT-SE-Named FL, GA, ENT-SE-Sunshine Peach, Large-AMER-Central-4                                                                                                                      | Katherine Evans, Patrick Byrne, Ruben Govender, David Wells                                      |
| AMER - East - Expand    | Steve Cull         | ENT-CE-Named Chicago, ENT-SE-Named MD, VA, ENT-CE-Named Ohio,  Valley                                                                                                                                                                        | Mark Bell,  Larry Biegel, TBH                                                    |
| AMER - East - Expand    | Shawn Winters      | Large-AMER-Northeast-2, Large-AMER-Northeast-4, Large-AMER-Southeast-3                                                                                                                                |  Paul Duffy, Tony Scafidi, Jim Berstein                            |
| AMER - East - Expand    | Max Chadliev       | Large-AMER-Northeast-3, Large-AMER-Central-1, ENT-SE-Named CGC, Large-AMER-Northeast-1                                                                                                                                                      | Peter McCracken, Tim Kuper, Sean Billow, TBH                                      |
| AMER - West - Expand    | Andrew Finegan       | ENT-NC-Named Santa Clara-1,  Large-AMER-Bay Area-1, ENT-NC-Named SF, Large-AMER-Southwest/                                                                                  | Nico Ochoa, Alyssa Belardi, Mike Nevolo, Rick Walker       |
| AMER - West - Expand    | Julia Hill-Wright                | ENT-SW-Named 1, Large-AMER-Midwest-1, ENT-SC-Named SD, ENT-MW-Named 1, ENT-MW-Named 2                                                                                                                    | Chris Cornacchia, Lydia Pollitt, Robert Hyry, Philip Wieczorek, TBH    |
| AMER - West - Expand    | Suzy Verdin        | Large-AMER-Bay Area-2, ENT-NC-Named Santa Clara 4, ENT-PNW-Named 2, Large-Amer-West-1, Large AMER-PNW/MW-1                                                                                                                            | Jim McMahon, Michael Scott, Alan Cooke, Adi Wolff, Haydn Mackay             |
| AMER - West - Expand    | Léa Tuizat                | ENT-PNW-Named 1, ENT-SW-Named UT, NV, NM, ENT-PNW-Named 2, ENT-NC-Named Santa Clara 2, ENT-SC-Named SD | Joe Drumtra, Steve Clark, TBH, Joe Miklos, Brad Downey                |
| EMEA -   NE  - Expand   | Shay Fleming       | Large-EMEA-UK/I-1, Large-EMEA-UK/I-3, Large-EMEA-UK/I-4, Large-EMEA-UK/I-7                                                                                                                                        | Annette Kristensen, Nasser Mohunol, Aslihan Kurt,  Steve Challis              |
| EMEA -  NE   - Expand   | Christos Lemos     | Large-EMEA-UK/I-2, NAMED-Peter Davies, Large-EMEA-UK/I-5, Large-EMEA-UK/I-6, Large-EMEA-UK/I-8                                                                                                                | Peter Davies, Justin Haley, Nicholas Lomas  |
| EMEA -  DACH   - Expand | Christina Souleles | Large-EMEA-DACH-1, NAMED-Timo Schuit                                                                                                                                 | Rene Hoferichter  Timo Schuit 
| EMEA -  DACH   - Expand | Lukas Mertensmeyer | Large-EMEA-DACH-2, Large-EMEA-DACH-3                                                                                    | Christoph Stahl  Dirk Dornseiff       |
| EMEA -  SE/EM - Expand  | Noria Aidam        | Large-EMEA-Large-EMEA-2, Large-EMEA-Large-EMEA-3, Large-EMEA-Large-EMEA-4, NAMED-Hugh Christey                                                                     | Vadim Rusin Philip Smith Anthony Seguillon                                 |
| EMEA -  SE/EM - Expand  | Tati Fernandez        | Large-EMEA-Large-EMEA-1, Large-EMEA-Large-EMEA-5, Large-EMEA-UK/I-9                            | Vadim Rusin, TBH, Godwill N'Dulor  


# Expectations

## Expand SDRs are responsible for:

- Accounts with their name in the `SDR Assigned` field
- Leading account planning efforts with SALs/TAMs/ISRs and creating in-depth account plans for top focus expand accounts and other accounts in their hierarchy
- Qualifying and converting marketing-generated leads (MQLs) and inquiries into sales accepted opportunities (SAOs)
- Aligning their outbound/inbound prospecting efforts to field/corporate/strategic/account based marketing campaigns
- Educating prospects and making positive impressions
- Generating/Expanding GitLab awareness, meetings, and ARR pipeline for sales
- Collaborating with peers, marketing, and sales teams


## Requirements:

- Maintain a high sense of autonomy to focus on what's most important
- Participate in lead planning, execution, and cross-functional meetings
- Participate in initial qualifying meetings, discovery calls, and follow-up conversations
- Maintain exceptional Salesforce hygiene, logging all prospecting activity, conversations and account intelligence uncovered, including LinkedIn conversations and calls as well as emails. 


# Working with Sales
Expand SDRs should prioritize communication with the SALs within territories they are aligned to. SDRs will organize and schedule a kickoff call to:
- Review the focus expand accounts (including all children/subsidiaries of over 500 employees) 
- Gather actionable intelligence from SALs/TAMs/ISRs and discuss key expand strategy
- Research the account with the SALs/TAMs/ISRs, see if LinkedIn introductions can be made
- Research the account on LISN and select 10 - 15 contacts who follow GitLab or who fall into the personas we target
- Review outbound SDR plays, campaigns, field tactics, and other offers
- Review [Outreach persona sequences](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#outbound-messaging) with SAL:  
- Document the plan using docs, a spreadsheet, and/or the SDR quarterly issue plan

Recurring SAL|SDR meetings can be scheduled or take place on an as-needed basis.

Slack is encouraged for informal communication (team slack channel), however, anything related to strategy, approach, or outreach should be documented.

# Expansion Strategies, Account Ranking, and Rules of Engagement

## Strategies:
- Growth within an existing customer account: SDR should strategically partner with SAL/ISR to identify additional business units to bring into the conversation as growth opportunities. Renewal dates should be exploited.
- Expansion into parent/children/subsidiaries of existing customer accounts: These are accounts that fall within the corporate hierarchy within Salesforce. If a child/subsidiary account is identified via Datafox but not in our system, then SDR should follow the following process:
Create a LEAD for that account and then convert to CONTACT to create the ACCOUNT when you qualify your first lead.
- Free to paid upgrades: Existing Core/CE users can be targeted to upgrade to a paid version of GitLab 

## [Ranking](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#definitions-for-enterprise-account-ranking) and prioritization of accounts:
- Rank 1: Expand with Purpose (EwP) accounts should be closely/strategically worked with SALs. These accounts have High Demandbase intent scores and should therefore be High touch by SDR.
- Rank 1.5: As above but nominated for ABM and should also be worked by SDRs
- Rank 2: ICP Ultimate parent accounts that have Core/CE-Users in hierarchy, Total CARR/LAM for account, Med/Low Demandbase intent scores and small renewals in current FQ.
- Rank 3: As above but are not our ICP

## Account Rules of Engagement:
- Any accounts that have an SDR’s name in the SDR Assigned field can be worked by the SDRs. Actively worked accounts should have the SR Target Account box checked.
- When accounts are flagged as “do not touch” by Sales, SDRs should identify these in their territory plans, but SDRs should still work the periphery: subsidiaries and business units that are not attached to the main GitLab customer instance. Only if the SAL is working on a Global License Agreement/MSA should an SDR not touch the account. If you run into pushback from a SAL, then please bring your manager into the conversation.
- SDRs can independently work any accounts they are allocated in SFDC, in addition to accounts your SALs request you to work.
- Enterprise SDRs should only work accounts owned by Enterprise SALs. If you think accounts are assigned incorrectly to a Mid-Market or SMB AE, then please Chatter the account owner, your SAL and Sales Support on the account record, provide a link with proof ownership should be a SAL, and see if it can be transferred to the SAL upon agreement by the commercial AE.

