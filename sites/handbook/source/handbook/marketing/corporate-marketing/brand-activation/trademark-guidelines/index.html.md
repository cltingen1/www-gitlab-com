---
layout: handbook-page-toc
title: "Trademark Guidelines"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Trademark Guidelines
{:.no_toc}

*Date of last revision: 2021-03-12*

Thank you for your interest in GitLab! You may want to use names, trademarks, logos, icons, slogans, or other identifiers owned by GitLab, Inc. or our affiliates (our “Trademarks”) in connection with business or promotional purposes permitted under these Guidelines (as defined below). Our Trademarks are valuable assets because our customers rely on our Trademarks to distinguish our products and services from those of other brands. By following these Guidelines, you help us protect our rights and reputation, and strengthen our corporate and brand identities.

### Scope and Applicability
Your use of our Trademarks must comply with these GitLab Trademark Guidelines and the GitLab [Brand Guidelines](/handbook/marketing/corporate-marketing/brand-activation/brand-standards/) which are incorporated into these GitLab Trademark Guidelines by reference (both are referred to collectively as these “Guidelines”). Any use of our Trademarks other than in accordance with these Guidelines is a violation of our rights and is strictly prohibited. If you (or the company by which you are employed) are a party to a written license or other agreement with GitLab, your agreement may include more specific or additional usage guidelines or terms governing your use of our Trademarks. If so, you must follow those guidelines or terms in your agreement in addition to those included in these Guidelines. If there’s a conflict between any terms in that other agreement and the terms of these Guidelines, the terms of that other agreement will govern.

### Authorized Uses
You have permission to use and reproduce our Trademarks on your products, point of sale materials, websites, promotional materials, trade show or educational materials, and talent acquisition materials (collectively, “Your Materials”) to promote your products’ compatibility with GitLab’s products, or to refer to GitLab or our products, in each case provided that such use is in compliance with these Guidelines.

Non-commercial use of the Trademarks is permitted under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).

### Usage Requirements and Restrictions
Except as expressly permitted by these Guidelines, you only have permission to use these [Trademarks](/handbook/marketing/corporate-marketing/brand-activation/brand-standards/) (the “Authorized Trademarks”), and no other Trademarks. Do not (i) make any changes to the Authorized Trademarks, including changes to their colors, typeface, fonts, graphic appearance, or proportions; (ii) use any partial versions of the Authorized Trademarks or any of your own versions of the Authorized Trademarks; and/or (iii) combine the Authorized Trademarks with any other logo, design, symbol, trademark, service mark or company name except as set forth below. Always use the Authorized Trademarks in compliance with these Guidelines.

#### Also, you must comply with these additional guidelines:

- Display the Authorized Trademarks in a manner that is consistent, undistorted, clear, and stands out from the surrounding text or graphics. Do not display an Authorized Trademark in a way that is so small that its design features are lost;

- Do not use the [Tanuki icon Trademark](/handbook/marketing/corporate-marketing/brand-activation/brand-standards/) as a standalone trademark for a first touchpoint, or combine our Tanuki icon Trademark with any other Trademark or element, except as combined in an Authorized Trademark;

- You may use Trademarks other than the Authorized Trademarks referentially or in descriptive copy, such as job descriptions or in tradeshow materials. If you are going to use one of our Trademarks in this way, use the Trademark as an adjective with an appropriate descriptor or generic term after the Trademark the first time it appears in the copy, and as often as is reasonable after that (for example, “looking for a developer with experience developing on the GitLab platform.”). <!-- [TO-DO] You can find suggested generic terms for some of our most commonly-used Trademarks in our Glossary and Usage guide here; and -->

- Never hyphenate, make a possessive of, pluralize, abbreviate, or change the capitalization of our Trademarks, or use our Trademarks as verbs. For example, do not refer to our platform as “gitlab”. <!-- [TO-DO] You can find the correct presentation of our Trademarks here. -->

If you want to use a Trademark in a manner that is not permitted by these Guidelines (e.g. your own stylized version of an Authorized Trademark), please reach out to `brand-and-digital@gitlab.com` for permission first.

### Unauthorized Use of our Trademarks
You may not use our Trademarks in violation of these Guidelines. Also, you may not use our Trademarks:

- in any way that suggests GitLab’s endorsement, sponsorship of, or affiliation with any of your products or services, unless expressly approved by GitLab in writing in each instance. For example, you may not use of any of our Trademarks in the name of a business, product, service, application, domain name, social media account, or other offering, or as a keyword in any advertising program;

- more prominently than your own trademarks or product or service name, or as the only source identifier (i.e. business name, brand, logo, or other trademark) on your products or promotional materials;

- in any manner that would damage or be detrimental to GitLab, or its products’ or services’, image, reputation, or goodwill; or

- in any manner that is deceptive, misleading, illegal or unethical.
Unauthorized use of our Trademarks may constitute trademark infringement, unfair competition, or violation of other applicable laws.

### Revocation
GitLab may revoke your rights to use our Trademarks at any time for any reason. If GitLab revokes such rights, you must immediately discontinue any and all use of our Trademarks, and deliver to GitLab or destroy or remove (and certify such destruction and/or removal in writing) all printed or online materials containing any of our Trademarks or any portion of our Trademarks. GitLab may also require you to make changes to your use of our Trademarks in order to continue distributing Your Materials or for any other purpose. You will promptly comply with any of those requested changes and correct any non-compliant use of the Trademarks, and provide written confirmation of such compliance to GitLab.

### Quality Control
You acknowledge that our Trademarks are associated with high standards of quality, and that it’s important to GitLab to maintain such high standards of quality for any products, services or other materials that bear our Trademarks. You agree that any of Your Materials that use our Trademarks will meet or exceed the quality standards of comparable GitLab products or services. If GitLab ever determines that any element of Your Materials or your use of our Trademarks does not meet a sufficient standard of quality, we reserve the right to terminate your right to continue to use our Trademarks at any time at our sole discretion.

### Ownership
GitLab retains all right, title and interest in and to our Trademarks, and you agree never to challenge, contest or question the validity of, or GitLab’s exclusive ownership of, our Trademarks. You agree that all uses of our Trademarks by you shall inure to the sole and exclusive benefit of GitLab. You will not file any trademark applications for any of our Trademarks or any confusingly similar trademark, service mark or logo in any country anywhere in the world or oppose or challenge, or assist anyone else in opposing or challenging, any of our Trademark applications or registrations.
You should also include the following trademark ownership language (which may be translated into a local language) in the credit notice section of any of Your Materials that include one or more of our Trademarks (or if not practical, related documentation):
GITLAB,  the GiLab logo and all other GitLab trademarks herein are the registered and unregistered trademarks of GitLab, Inc.

### If You See Something…
Do us a favor - if you see someone use a name, trademark, logo, icon, slogan, or other identifier that is identical or similar to any of our Trademarks and that may cause confusion in the marketplace or may be fraudulent, please let us know. Giving us a heads-up about that kind of use will help us maintain the integrity of our Trademarks and protect our customers. Please reach out to `brand-and-digital@gitlab.com` to report misuse. 

### Still have questions?
Please email `brand-and-digital@gitlab.com` with additional inquiries. 

### Escalating Requests (GitLab Team Members Only)
Need a trademark usage request form signed or more support from procurement on a specific request? Please start an issue in the procurement project using the [trademark usage request issue template](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=trademark_usage_request).
