# frozen_string_literal: true

require 'middleman'
require 'json'

# This extension currently resolves the following issue: https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1070,
# which is part of a broader plan to mitigate this incident: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/4969.
#
# Eventually, this should also handle all of our assets, via: https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1420.
# That work will require Webpack changes, and integration of those changes into this extension (or some other mechanism for matching the hashes).
#
# Since our Webpack config isn't currently bundling anything, and the references to those files are inconsistent throughout the repository,
# starting small with the Slippers assets will resolve the bulk of the cache busting needs, and provide a framework moving forward for better cache busting.
#
# We can't use the [asset_hash](https://middlemanapp.com/advanced/improving-cacheability/#uniquely-named-assets) built in to Middleman because we are using the external pipeline to handle our assets.
class CacheBusting < Middleman::Extension
  # The ready function hooks into the ready callback provided by Middleman, then it retrieves the asset version from `package.json` at the root of the repository.
  # It makes that version number available in @app.config, for use in any template that requires it.
  def ready
    @asset_version = get_asset_version
    @app.config[:asset_version] = @asset_version
  end

  # Read the version number from package.json
  def get_asset_version
    JSON.parse(File.read(File.expand_path('../package.json', __dir__)))['dependencies']['slippers-ui'].tr('.', '-')
  end
end

::Middleman::Extensions.register(:cache_busting, CacheBusting)
