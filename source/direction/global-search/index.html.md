---
layout: markdown_page
title: "Category Direction - Global Search"
description: "GitLab supports Advanced Search for GitLab.com and Self-Managed instances. This provides users with a faster and more complete search."
canonical_path: "/direction/global-search/"
---

- TOC
{:toc}

## Global Search

| | |
| --- | --- |
| Section | [Enablement](/direction/enablement/) |
| Content Last Reviewed | `2021-07-30` |

### Introduction

Thank you for visiting this direction page on Global Search in GitLab. This page belongs to the [Global Search](/handbook/product/categories/#global-search-group) group of the Enablement stage.

 Share feedback directly via [Twitter](https://twitter.com/JohnMcG21038614), or on a [video call](https://calendly.com/jmcguire-gitlab). If you're a GitLab user and have direct knowledge of your need for search, we'd especially love to hear from you.

### Overview

Finding anything in GitLab should be easy. If it’s easy users will be more likely to contribute as well as get more use out of what was already contributed.  

Global Search is the core search feature for GitLab, as the one place to search for anything in GitLab. “Global” refers to the ability to search across the project, group, or instance level, as well as across content types. Users can search issues, merge requests, commits, epics, projects, wikis, comments, and users.

There are two modes Global Search can operate in, depending on the instance configuration:

* Basic Search is the default search mode for a new instance of GitLab, requiring no additional configuration. This is focused on the experience for instances with 10-30 users total. 
* Advanced Search is available at the Premium tier, and provides a richer search experience. An Elasticsearch cluster is required, which is not packaged by GitLab today. Advanced Search allows cross-project and group code search, is faster than Basic Search, and provides more search options.  

### Vision 

* Create a search experience that allows the streamlining of workflows for all individuals and teams across all stages of the DevOps lifecycle. 
* Enabling toolchain consolidation by including all ten stages of the DevOps lifecycle in Global Search
* Improve options and ease of deployment for Advanced Search in all instances


#### Objectives

* [Make Elasticsearch available to Free and Core for Issues, Merge Requests, Projects, Users, and Commits](https://gitlab.com/groups/gitlab-org/-/epics/6222). 
* Improve the User Experiance to make Gitlab content more explorable. 
* Allow the searching and exploring of contetnt across projects and Groups. 

### 2022 Goals FY23 / 12 month plan

- [Make Elasticsearch avilable to Free and Core for Issues, Merge Requests, Projects, Users, and Commits](https://gitlab.com/groups/gitlab-org/-/epics/6222). 
- [Improve UX of Global Search to take advantage of features.](https://gitlab.com/groups/gitlab-org/-/epics/4084) 
- [Re-architect for efficiency and move to managing our own stack](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/311)
- [Federated Search for GitLab Self-managed and GitLab.com ](https://gitlab.com/groups/gitlab-org/-/epics/6221)
- [Solve for elastic scaling that can handle continuous growth](https://gitlab.com/groups/gitlab-org/-/epics/6248)

### Desired Results

Global Search [Unique Monthly Users](https://about.gitlab.com/handbook/product/enablement-section-performance-indicators/#enablementglobal-search---paid-gmau---the-number-of-unique-paid-users-per-month) should grow at 5% MoM as we continue to see features and use cases enabled. 
If Monthly Users continue to grow this will be seen as a verification that we are delivering a good rate of growth. If we see this decline in a consistent fashion it could be seen as a sign that correction in direction is warranted. 

#### Measuring Success - Target PI's / Metrics

We look at unique users per month, GMAU.  Our goal is to grow this 5% each month. 

We chronicled our journey of deploying Elasticsearch for GitLab.com through several blog posts.
* [2019-03-20 Lessons from our journey to enable global code search with Elasticsearch on GitLab.com](/blog/2019/03/20/enabling-global-search-elasticsearch-gitlab-com/)
* [2019-07-16 Update: The challenge of enabling Elasticsearch on GitLab.com](/releases/2019/07/16/elasticsearch-update/)
* [2020-04-28 Update: Elasticsearch lessons learnt for Advanced Search](/blog/2020/04/28/elasticsearch-update/)
* [2021-06-01 GitLab's data migration process for Advanced Search](https://about.gitlab.com/blog/2021/06/01/advanced-search-data-migrations/)

### Target Audience and Experience 
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category. An overview
of the evolving user journeys as the category progresses through minimal, viable, complete and lovable maturity levels.-->


Advanced Search is targeted at all personas who might use GitLab in Development Organizations with more than 10 Projects active. 

### Maturity

Currently, GitLab's maturity for Search is "viable".

GitLab's current Advanced Search experience works for self-managed instances and across all Paid SaaS Experiances. 
UX Research, and SUS scores have shown a direct need improve specific aspects of the UI and functionality of search capabilites. 

Moving Global Search to the Maturity stage "Complete" will require these changes. 
* Complete JTBD Analysis 
* Complete UX Scorecard for Baseline
* Obtain Scores that show Efficient Level to promote the maturity to Complete. 

### What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top priorities for a few stakeholders. This section must provide a link to an issue or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

We have added a live roadmap that will track the progress as we complete major epics.
[Global Search Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aglobal%20search&label_name[]=Roadmap)

### What is Not Planned right now

Currently there is not a plan to scale beyond the needs of Paid Groups on GitLab.com. This means that while the ambition of the Search Group is to expand Advanced Search to all users of GitLab, we're not yet ready to move in that direction. We will continue to add features to Basic search as we can while evolving Advanced Search. 

### The Future of Global Search

As GitLab continues to grow it will become important to keep growing with GitLab. One of these areas that Global search will have impact is in adding new content types. Currently, this includes adding [Vulnerabilities](https://gitlab.com/gitlab-org/gitlab/-/issues/233733), and [Snippets](https://gitlab.com/gitlab-org/gitlab/-/issues/20963). 

We will also be Allow be planning to offer deeper integration across other list pages in Gitlab. Lucene based indexing allows for very fast return of structured lists there are several pages in GitLab that offer these list and it’s a continual improvement to improve the speed. 

Increase the number of Self-Managed users, using Advanced Search. We are considering how to enable [Advanced Search by default](https://gitlab.com/gitlab-com/Product/-/issues/1716). We have already made Advanced Search part of SaaS for all paid users. 

### Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk about, but we’re talking with customers about what they actually use, and ultimately what they need.-->

Both GitHub and BitBucket provide a more comprehensive and complete search for users; particularly in their ability to deeply search code and surface those results to users. 

There is a great need to improve the ability to search across repos for all competitors. This is commonly asked about from our prospective customers and is included in the comparison matrix for GitLab as well as the ROI calculator. 

| Feature 	| GitLab 	| SourceGraph 	| GitHub 	| BitBucket 	| JIRA 	|
|-	|-	|-	|-	|-	|-	|
| Keyword Search <br>   Search with keywords not specifying a field 	| **YES** 	| **YES** 	| **YES** 	|  	|  No 	|
| Search Filters<br>   Filters can reduce results to more specific results 	| **YES** 	| **YES** 	| **YES** 	|  	| **YES** 	|
| NLP (Natural Language Processing)<br>   Search uses Linguistic structure to improve matching 	| **YES** 	| **YES** 	| **YES** 	|  	|  No 	|
| Language Code Filters <br>   Search detects Code Language and allows filtering to a specific Language 	|  No 	| **YES** 	| **YES** 	|  	|  	|
| Search Diffs <br>   Search returns results from previous versions 	|  No 	| **YES** 	| No 	|  	|  	|
| Commit message <br>   Search by text used for the Commit message 	| **YES** 	| **YES** 	|  **YES** 	|  	|  	|
| Saved searches <br>   The search with parameters can be saved and favorited to show later  	|  No 	| **YES** 	| No 	|  	|  	|
| Custom filter groupings <br>   Combine filters and save them as a filter grouping 	|  No 	| **YES** 	| No 	|  	|  	|
| Autocomplete <br>   Keywords are recommended while you are typing 	| **YES** 	| **YES** 	|  No 	|  	|  	|
| Rank statistics<br>   Results show a score variable between the results 	|  No 	| **YES** 	|  No 	|  	|  	|
| Multiple Branches <br>   Code search can show more than the main branch 	|  No 	| **YES** 	|  No 	|  	|  	|
| Compare results <br>   Result page allows you to compare a result to other results from other searches 	|  No 	|  No 	|  No 	|  	|  	|
| Comments <br>   Results are shown from comments 	| **YES** 	|  No 	|  **YES** 	|  	|  	|
| All Results <br>  Results are shown across all scopes or types in one integrated result page 	|  No 	| **YES** 	|  No 	|  	|  	|
| Keyboard Shortcuts <br>   Search is quickly accessible for key commands 	| **YES** 	| **YES** 	|  No  	|  	|  	|
| ID Quick Search <br>   Search Recognizes IDs and takes you directly to the item page 	| **YES** 	|  No 	|  No 	|  	|  	|
| Search by SHA<br>   Commit SHAs are recognized and you are directed to the sha instead of the results 	| **YES** 	|  No 	| No  	|  	|  	|
| File Search <br>   Files and directories are identified and searchable not just the reference of a file listed in code	| **YES** 	|  No 	| **YES** 	|  	|  	|
| SaaS <br>   Featurefull search is offered as part of the SaaS offering.  	| **YES** 	|  No 	| **YES** 	|  	|  	|
| APIs<br>   Users can query search using APIs directly 	| **YES** 	|  No 	| **YES** 	|  	|  	|
| Issues<br>   Issues are searchable 	| **YES** 	|  No 	| **YES** 	|  	|  	|
| Merge Request<br>   Merge requests are searchable (not just the code 	| **YES** 	|  No 	| **YES** 	|  	|  	|
| Wiki<br>   Wikis are searchable 	| **YES** 	|  No 	| **Yes** 	|  	|  	|
| Epics<br>   Epics are searchable 	| **YES** 	|  No 	| No	|  	|  	|
| Security Vulnerability<br>   Search results for vulnerabilites 	|  No 	|  No 	| No 	|  	|  	|
| Snipets<br>   Snipets are searchable 	|  No 	|  No 	|  No 	|  	|  	|
| Batch Changes<br>   Start batch changes from a result list 	|  No 	| **YES** 	| No 	|  	|  	|
| Search Across Groups<br>   Search across groups, Global Search 	| **YES** 	| **YES** 	| **YES** 	|  	|  	|
| COST <br>   Per seat license (avg) 	| **Included** 	| **$11** 	| **Included** 	|  	|  	|


<!-- ### Analyst Landscape -->
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues that will help us stay relevant from their perspective.-->

<!-- ### Top Customer Success/Sales issue(s) -->
<!-- These can be sourced from the CS/Sales top issue labels when available, internal surveys, or from your conversations with them.-->

### Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most thumbs-up), but you may have a different item coming out of customer calls.-->

Velocity will be decreased starting with 14.1 as we contiiune to focus on staffing the Global Search Group. 

### Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

Velocity will be decreased starting with 14.1 as we contiiune to focus on staffing the Global Search Group. 

- [Performance Improvements for Elasticsearch Latency](https://gitlab.com/groups/gitlab-org/-/epics/5240)

### Top Strategy Item(s)
<!-- What's the most important thing to move your vision forward?-->

- [Create Clode Search Category](https://gitlab.com/gitlab-com/Product/-/issues/2575)
- [Global Search Category Maturity](https://gitlab.com/groups/gitlab-com/-/epics/1271)
